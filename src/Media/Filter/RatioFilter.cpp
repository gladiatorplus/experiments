
#include "opencv2/opencv.hpp"
#include "RatioFilter.h"

RatioFilter::RatioFilter() {
}

RatioFilter::~RatioFilter() {
	Stop();
}

bool RatioFilter::Start(MediaFilterParams & params){
	m_params = params;
	return true;
}

int RatioFilter::InputData(MediaData * data){
	MediaDataVideoBaseBand * baseband = static_cast<MediaDataVideoBaseBand *>(data);

	cv::UMat source;
	if(baseband->fmt == VideoBaseBandFmt::YUV420P){
		cv::Mat souce_host(baseband->height * 3 / 2,baseband->width,CV_8UC1,baseband->buffer[0]);
		souce_host.copyTo(source);
	}else{
		return false;
	}

	cv::cvtColor(source, source, cv::COLOR_YUV2BGR_I420);

	int src_w = source.cols;
	int src_h = source.rows;

	int dst_w = m_params.ratio.width, dst_h = m_params.ratio.height;
	cv::UMat dst = cv::UMat(cv::Size(dst_w, dst_h), CV_8UC3, cv::Scalar(0,0,0));

	if(src_w > src_h) {
		cv::UMat temp;
		float ratio = (float)dst_w/src_w;
		float inner_h = src_h * ratio;
		bool oversize = false;
		if(inner_h >= dst_h){
			inner_h = dst_h;
			oversize = true;
		}
		cv::resize(source,temp,cv::Size(dst_w,inner_h),0,0,cv::INTER_LINEAR);

		cv::UMat image_ROI = dst(cv::Rect(0, dst_h / 2 - inner_h / 2, temp.cols, temp.rows));
		temp.copyTo(image_ROI);

		if(m_params.ratio.blur && !oversize){

			cv::Size blur_ksize = cv::Size(40,40);

			cv::UMat source_ROI = source(cv::Rect(src_w / 4,src_h / 4, src_w / 4 * 3, src_h / 4 * 3));

			cv::UMat top_source_ROI = source_ROI(cv::Rect(0, 0, source_ROI.cols , source_ROI.rows / 2));
			cv::blur(top_source_ROI,top_source_ROI,blur_ksize);
			cv::resize(top_source_ROI,top_source_ROI,cv::Size(dst_w,dst_h / 2 - inner_h / 2),0,0,cv::INTER_LINEAR);
			cv::UMat top_ROI = dst(cv::Rect(0, 0, dst_w, dst_h / 2 - inner_h / 2));
			top_source_ROI.copyTo(top_ROI);

			cv::UMat botton_source_ROI = source_ROI(cv::Rect(0, source_ROI.rows / 2, source_ROI.cols , source_ROI.rows / 2));
			cv::blur(botton_source_ROI,botton_source_ROI,blur_ksize);
			cv::resize(botton_source_ROI,botton_source_ROI,cv::Size(dst_w,dst_h / 2 - inner_h / 2),0,0,cv::INTER_LINEAR);
			cv::UMat botton_ROI = dst(cv::Rect(0, dst_h / 2 + inner_h / 2, dst_w, dst_h / 2 - inner_h / 2));
			botton_source_ROI.copyTo(botton_ROI);
		}
	} else {
		cv::UMat temp;
		float ratio = (float)dst_h/(float)src_h;
		float inner_w = src_w * ratio;
		bool oversize = false;
		if(inner_w >= dst_w){
			inner_w = dst_w;
			oversize = true;
		}
		cv::resize(source,temp,cv::Size(inner_w,dst_h),0,0,cv::INTER_LINEAR);

		cv::UMat image_ROI = dst(cv::Rect(dst_w / 2 - inner_w / 2, 0, temp.cols, temp.rows));
		temp.copyTo(image_ROI);

		if(m_params.ratio.blur && !oversize){

			cv::Size blur_ksize = cv::Size(40,40);

			cv::UMat source_ROI = source(cv::Rect(src_w / 4,src_h / 4, src_w / 4 * 3, src_h / 4 * 3));

			cv::UMat left_source_ROI = source_ROI(cv::Rect(0, 0,source_ROI.cols / 2 , source_ROI.rows));
			cv::blur(left_source_ROI,left_source_ROI,blur_ksize);
			cv::resize(left_source_ROI,left_source_ROI,cv::Size(dst_w / 2 - inner_w / 2,dst_h),0,0,cv::INTER_LINEAR);
			cv::UMat left_ROI = dst(cv::Rect(0,0,dst_w / 2 - inner_w / 2,dst_h));
			left_source_ROI.copyTo(left_ROI);

			cv::UMat right_source_ROI = source_ROI(cv::Rect(source_ROI.cols / 2, 0,source_ROI.cols / 2 , source_ROI.rows));
			cv::blur(right_source_ROI,right_source_ROI,blur_ksize);
			cv::resize(right_source_ROI,right_source_ROI,cv::Size(dst_w / 2 - inner_w / 2,dst_h),0,0,cv::INTER_LINEAR);
			cv::UMat right_ROI = dst(cv::Rect(dst_w / 2 + inner_w / 2,0,dst_w / 2 - inner_w / 2,dst_h));
			right_source_ROI.copyTo(right_ROI);
		}
	}

	cv::cvtColor(dst, dst, cv::COLOR_BGR2YUV_I420);

	cv::Mat dst_host;
	dst.copyTo(dst_host);

	MediaDataVideoBaseBand baseband_data;
	int y_size = m_params.ratio.width * m_params.ratio.height;
	int uv_size = y_size / 4;
	baseband_data.buffer[0] = dst_host.data;
	baseband_data.line_size[0] = m_params.ratio.width;
	baseband_data.buffer[1] = dst_host.data + y_size;
	baseband_data.line_size[1] = m_params.ratio.width / 2;
	baseband_data.buffer[2] = dst_host.data + y_size + uv_size;
	baseband_data.line_size[2] = m_params.ratio.width / 2;
	baseband_data.fmt = VideoBaseBandFmt::YUV420P;
	baseband_data.width = m_params.ratio.width;
	baseband_data.height = m_params.ratio.height;
	baseband_data.pts = baseband->pts;
	m_cb(&baseband_data,m_user_data);

	return true;
}
bool RatioFilter::Stop(){
	return true;
}
