

#include "FrameRateFilter.h"

extern "C"{
#include <libavfilter/avfilter.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
}

FrameRateFilter::FrameRateFilter() {
	// TODO Auto-generated constructor stub
}

FrameRateFilter::~FrameRateFilter() {
	// TODO Auto-generated destructor stub
	Stop();
}

bool FrameRateFilter::Start(MediaFilterParams & params){
	Stop();
	if(!m_output_frame){
		m_output_frame=av_frame_alloc();
	}
	char args[512]={0};
	const AVFilter *buffersrc  = avfilter_get_by_name("buffer");
	const AVFilter *buffersink = avfilter_get_by_name("buffersink");
	AVFilterInOut *outputs = avfilter_inout_alloc();
	AVFilterInOut *inputs = avfilter_inout_alloc();
	AVPixelFormat pix_fmts[2];
	pix_fmts[1] = AV_PIX_FMT_NONE;
	switch (params.source_params.video.pix_fmt) {
		case VideoBaseBandFmt::YUV420P:
			pix_fmts[0] = AV_PIX_FMT_YUV420P;
			break;
		default:
			return false;
	}
	sprintf(args,
			"video_size=%dx%d:pix_fmt=%d:time_base=1/1000:pixel_aspect=1/1",
			params.source_params.video.width,params.source_params.video.height,
			pix_fmts[0]);
	if(!m_input_frame){
		m_input_frame = av_frame_alloc();
		m_input_frame->width = params.source_params.video.width;
		m_input_frame->height = params.source_params.video.height;
		m_input_frame->format = pix_fmts[0];
	}
	m_filter_graph = avfilter_graph_alloc();
	int ret = avfilter_graph_create_filter(&m_buffersrc_ctx, buffersrc, "in",
		args, nullptr, m_filter_graph);
	if (ret < 0) {
		return false;
	}
	outputs->name = av_strdup("in");
	outputs->filter_ctx = m_buffersrc_ctx;
	outputs->pad_idx = 0;
	outputs->next = nullptr;

	AVBufferSinkParams * buffersink_params = av_buffersink_params_alloc();
	buffersink_params->pixel_fmts = pix_fmts;
	ret = avfilter_graph_create_filter(&m_buffersink_ctx, buffersink, "out",
		nullptr, buffersink_params, m_filter_graph);
	av_free(buffersink_params);
	if (ret < 0) {
		return false;
	}

	ret = av_opt_set_int_list(m_buffersink_ctx, "pix_fmts", pix_fmts,
							  AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN);
	if (ret < 0) {
		return false;
	}

	inputs->name = av_strdup("out");
	inputs->filter_ctx = m_buffersink_ctx;
	inputs->pad_idx = 0;
	inputs->next = nullptr;

	if(params.framerate.frame_rate_den <= 0 || params.framerate.frame_rate_num <= 0){
		return false;
	}

	double framerate = (double)params.framerate.frame_rate_num / (double)params.framerate.frame_rate_den;

	char filter_descr[4096]={0};
	sprintf(filter_descr,"fps=fps=%lf",framerate);

	if ((ret = avfilter_graph_parse_ptr(m_filter_graph, filter_descr,
		&inputs, &outputs, nullptr)) < 0){
		return false;
	}

	if ((ret = avfilter_graph_config(m_filter_graph, nullptr)) < 0){
		return false;
	}

	if(!m_video_frame_buffer)
		m_video_frame_buffer = new unsigned char[7680*4320*2];

	m_last_pts = -1;

	return true;
}

int FrameRateFilter::InputData(MediaData * data){
	if(!m_buffersink_ctx)
		return -1;
	MediaDataVideoBaseBand * baseband = static_cast<MediaDataVideoBaseBand *>(data);

	switch (baseband->fmt) {
		case VideoBaseBandFmt::YUV420P: {
			m_input_frame->format = AV_PIX_FMT_YUV420P;
			if(!baseband->buffer[0] || !baseband->buffer[1] || !baseband->buffer[2])
				return false;
			avpicture_fill((AVPicture*)m_input_frame, nullptr, (AVPixelFormat)m_input_frame->format, m_input_frame->width, m_input_frame->height);
			m_input_frame->pts = baseband->pts;
			m_input_frame->data[0] = baseband->buffer[0];
			m_input_frame->data[1] = baseband->buffer[1];
			m_input_frame->data[2] = baseband->buffer[2];
			break;
		}
		default:
			return false;
	}

	if (av_buffersrc_add_frame(m_buffersrc_ctx, m_input_frame) < 0) {
		return -1;
	}

	while (1) {
		int ret = av_buffersink_get_frame(m_buffersink_ctx, m_output_frame);
		if(ret < 0 ){
			if (ret == AVERROR(EAGAIN) || ret == AVERROR(AVERROR_EOF))
				return -1; //means need more frames
			else
				return -2; //means other failure cases
		}
		else{
			MediaDataVideoBaseBand baseband_data;
			if(m_output_frame->format == AV_PIX_FMT_YUV420P){
				int source_y = 0,source_u = 0,source_v =0;
				int dst_y = 0,dst_u = m_output_frame->width * m_output_frame->height,dst_v = m_output_frame->width * m_output_frame->height * 5 / 4;
				for(int i=0;i<m_output_frame->height;i++){
					memcpy(m_video_frame_buffer + dst_y,m_output_frame->data[0] + source_y,m_output_frame->width);
					dst_y += m_output_frame->width;
					source_y += m_output_frame->linesize[0];
					if(i < m_output_frame->height / 2){
						memcpy(m_video_frame_buffer + dst_u,m_output_frame->data[1] + source_u,m_output_frame->width / 2);
						memcpy(m_video_frame_buffer + dst_v,m_output_frame->data[2] + source_v,m_output_frame->width / 2);
						dst_u += m_output_frame->width / 2;
						dst_v += m_output_frame->width / 2;
						source_u += m_output_frame->linesize[1];
						source_v += m_output_frame->linesize[2];
					}
				}
				baseband_data.buffer[0] = m_video_frame_buffer;
				baseband_data.line_size[0] = m_output_frame->width;
				baseband_data.buffer[1] = m_video_frame_buffer + m_output_frame->width * m_output_frame->height;
				baseband_data.line_size[1] = m_output_frame->width / 2;
				baseband_data.buffer[2] = m_video_frame_buffer + m_output_frame->width * m_output_frame->height * 5 / 4;
				baseband_data.line_size[2] = m_output_frame->width / 2;
				baseband_data.fmt = VideoBaseBandFmt::YUV420P;
				baseband_data.width = m_output_frame->width;
				baseband_data.height = m_output_frame->height;
				AVRational framerate = av_buffersink_get_frame_rate(m_buffersink_ctx);
				if(m_last_pts == -1){
					baseband_data.pts = baseband->pts;
					m_last_pts = baseband_data.pts;
				}else{
					double current_pts = m_last_pts + (double)framerate.den * 1000 / (double)framerate.num;;
					baseband_data.pts = current_pts;
					m_last_pts = current_pts;
				}
			}
			m_cb(&baseband_data,m_user_data); //frame was successfully returned
			av_frame_unref(m_output_frame);
		}
	}
	return 0;
}
bool FrameRateFilter::Stop(){
	if(m_filter_graph){
		avfilter_graph_free(&m_filter_graph);
		m_filter_graph = nullptr;
	}
	if(m_output_frame){
		av_frame_free(&m_output_frame);
		m_output_frame = nullptr;
	}
	if(m_input_frame){
		av_frame_free(&m_input_frame);
		m_input_frame = nullptr;
	}
	if(m_video_frame_buffer){
		delete [] m_video_frame_buffer;
		m_video_frame_buffer = nullptr;
	}
	return true;
}
