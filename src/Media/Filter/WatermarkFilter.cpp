

#include "WatermarkFilter.h"

extern "C"{
#include <libavfilter/avfilter.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>
}

WatermarkFilter::WatermarkFilter() {
}

WatermarkFilter::~WatermarkFilter() {
	Stop();
}

bool WatermarkFilter::Start(MediaFilterParams & params){
	if(!m_output_frame){
		m_output_frame=av_frame_alloc();
	}
	char args[512]={0};
	const AVFilter *buffersrc  = avfilter_get_by_name("buffer");
	const AVFilter *buffersink = avfilter_get_by_name("buffersink");
	if(!m_outputs){
		m_outputs = avfilter_inout_alloc();
	}
	if(!m_inputs){
		m_inputs = avfilter_inout_alloc();
	}
	AVPixelFormat pix_fmts[2];
	pix_fmts[1] = AV_PIX_FMT_NONE;

	switch (params.source_params.video.pix_fmt) {
		case VideoBaseBandFmt::YUV420P:
			pix_fmts[0] = AV_PIX_FMT_YUV420P;
			break;
		default:
			return false;
	}
	sprintf(args,
		"video_size=%dx%d:pix_fmt=%d:time_base=1/1000:pixel_aspect=1/1",
		params.source_params.video.width,params.source_params.video.height,
		pix_fmts[0]);
	if(!m_input_frame){
		m_input_frame = av_frame_alloc();
		m_input_frame->width = params.source_params.video.width;
		m_input_frame->height = params.source_params.video.height;
		m_input_frame->format = pix_fmts[0];
	}

	m_filter_graph = avfilter_graph_alloc();
	int ret = avfilter_graph_create_filter(&m_buffersrc_ctx, buffersrc, "in",
		args, nullptr, m_filter_graph);
	if (ret < 0) {
		return false;
	}
	m_outputs->name = av_strdup("in");
	m_outputs->filter_ctx = m_buffersrc_ctx;
	m_outputs->pad_idx = 0;
	m_outputs->next = nullptr;

	AVBufferSinkParams * buffersink_params = av_buffersink_params_alloc();
	buffersink_params->pixel_fmts = pix_fmts;
	ret = avfilter_graph_create_filter(&m_buffersink_ctx, buffersink, "out",
		nullptr, buffersink_params, m_filter_graph);
	av_free(buffersink_params);
	if (ret < 0) {
		return false;
	}

	ret = av_opt_set_int_list(m_buffersink_ctx, "pix_fmts", pix_fmts,
							  AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN);
	if (ret < 0) {
		return false;
	}

	m_inputs->name = av_strdup("out");
	m_inputs->filter_ctx = m_buffersink_ctx;
	m_inputs->pad_idx = 0;
	m_inputs->next = nullptr;

	char filter_descr[4096]={0};
	if(params.watermarks.size() > 0){
		int watermarks_num = params.watermarks.size();
		char path[1024] = {0};
		char position[1024] = {0};
		for(int i=0;i<watermarks_num;i++){
			auto wm = params.watermarks[i];
			sprintf(path + strlen(path),"movie='%s',scale=%s[wm%d];",wm.pic_path.c_str(),wm.size.c_str(),i);
			if(params.watermarks.size() == 1)
				sprintf(position + strlen(position),"[wm%d]overlay=%s",i,wm.position.c_str());
			else{
				if(i == 0){
					sprintf(position + strlen(position),"[wm%d]overlay=%s[tmp]",i,wm.position.c_str());
				}else if(i == watermarks_num - 1){
					sprintf(position + strlen(position),",[tmp][wm%d]overlay=%s",i,wm.position.c_str());
				}else{
					sprintf(position + strlen(position),",[tmp][wm%d]overlay=%s[tmp]",i,wm.position.c_str());
				}
			}
		}
		sprintf(filter_descr,"%s[in]%s[out]",path,position);
	}else{
		return false;
	}

	if ((ret = avfilter_graph_parse_ptr(m_filter_graph, filter_descr,
		&m_inputs, &m_outputs, nullptr)) < 0){
		return false;
	}

	if ((ret = avfilter_graph_config(m_filter_graph, nullptr)) < 0){
		return false;
	}

	if(!m_video_frame_buffer)
		m_video_frame_buffer = new unsigned char[7680*4320*2];

	return true;
}

int WatermarkFilter::InputData(MediaData * data){
	if(!m_buffersink_ctx)
		return -1;
	MediaDataVideoBaseBand * baseband = static_cast<MediaDataVideoBaseBand *>(data);

	switch (baseband->fmt) {
		case VideoBaseBandFmt::YUV420P: {
			m_input_frame->format = AV_PIX_FMT_YUV420P;
			if(!baseband->buffer[0] || !baseband->buffer[1] || !baseband->buffer[2])
				return false;
			avpicture_fill((AVPicture*)m_input_frame, nullptr, (AVPixelFormat)m_input_frame->format, m_input_frame->width, m_input_frame->height);
			m_input_frame->pts = baseband->pts;
			m_input_frame->data[0] = baseband->buffer[0];
			m_input_frame->data[1] = baseband->buffer[1];
			m_input_frame->data[2] = baseband->buffer[2];
			break;
		}
		default:
			return false;
	}

	if (av_buffersrc_add_frame(m_buffersrc_ctx, m_input_frame) < 0) {
		return -1;
	}

	while (1) {
		int ret = av_buffersink_get_frame(m_buffersink_ctx, m_output_frame);
		if(ret < 0 ){
			if (ret == AVERROR(EAGAIN) || ret == AVERROR(AVERROR_EOF))
				return -1; //means need more frames
			else
				return -2; //means other failure cases
		}
		else{
			MediaDataVideoBaseBand baseband_data;
			if(m_output_frame->format == AV_PIX_FMT_YUV420P){
				int source_y = 0,source_u = 0,source_v =0;
				int dst_y = 0,dst_u = m_output_frame->width * m_output_frame->height,dst_v = m_output_frame->width * m_output_frame->height * 5 / 4;
				for(int i=0;i<m_output_frame->height;i++){
					memcpy(m_video_frame_buffer + dst_y,m_output_frame->data[0] + source_y,m_output_frame->width);
					dst_y += m_output_frame->width;
					source_y += m_output_frame->linesize[0];
					if(i < m_output_frame->height / 2){
						memcpy(m_video_frame_buffer + dst_u,m_output_frame->data[1] + source_u,m_output_frame->width / 2);
						memcpy(m_video_frame_buffer + dst_v,m_output_frame->data[2] + source_v,m_output_frame->width / 2);
						dst_u += m_output_frame->width / 2;
						dst_v += m_output_frame->width / 2;
						source_u += m_output_frame->linesize[1];
						source_v += m_output_frame->linesize[2];
					}
				}
				baseband_data.buffer[0] = m_video_frame_buffer;
				baseband_data.line_size[0] = m_output_frame->width;
				baseband_data.buffer[1] = m_video_frame_buffer + m_output_frame->width * m_output_frame->height;
				baseband_data.line_size[1] = m_output_frame->width / 2;
				baseband_data.buffer[2] = m_video_frame_buffer + m_output_frame->width * m_output_frame->height * 5 / 4;
				baseband_data.line_size[2] = m_output_frame->width / 2;
				baseband_data.fmt = VideoBaseBandFmt::YUV420P;
				baseband_data.width = m_output_frame->width;
				baseband_data.height = m_output_frame->height;
				baseband_data.pts = m_output_frame->pts;
			}
			m_cb(&baseband_data,m_user_data); //frame was successfully returned
			av_frame_unref(m_output_frame);
		}
	}
	return 0;
}
bool WatermarkFilter::Stop(){
	if(m_filter_graph){
		avfilter_graph_free(&m_filter_graph);
		m_filter_graph = nullptr;
	}
	if(m_output_frame){
		av_frame_free(&m_output_frame);
		m_output_frame = nullptr;
	}
	if(m_input_frame){
		av_frame_free(&m_input_frame);
		m_input_frame = nullptr;
	}
	if(m_video_frame_buffer){
		delete [] m_video_frame_buffer;
		m_video_frame_buffer = nullptr;
	}
	if(m_outputs){
		avfilter_inout_free(&m_outputs);
		m_outputs = nullptr;
	}
	if(m_inputs){
		avfilter_inout_free(&m_inputs);
		m_inputs = nullptr;
	}
	return true;
}
