#include "include/cef_client.h"
#include "include/cef_frame.h"
#include "include/cef_string_visitor.h"
#include "include/cef_base.h"
#include "include/cef_app.h"

int main(int argc,char** argv){
	CefMainArgs main_args(argc,argv);
	return CefExecuteProcess(main_args, nullptr, nullptr);
}
