#include "CropFilter.h"  
	
#include "opencv2/opencv.hpp"

CropFilter::CropFilter(){	
}
	
CropFilter::~CropFilter(){	
    Stop();
}


bool CropFilter::Start(MediaFilterParams & params){
    m_params = params;
    return true;
}
int CropFilter::InputData(MediaData * data){
    MediaDataVideoBaseBand * baseband = static_cast<MediaDataVideoBaseBand*>(data);
    cv::UMat source;
	if(baseband->fmt == VideoBaseBandFmt::YUV420P){
		cv::Mat souce_host(baseband->height * 3 / 2,baseband->width,CV_8UC1,baseband->buffer[0]);
		souce_host.copyTo(source);
	}else{
		return false;
	}

    cv::cvtColor(source, source, cv::COLOR_YUV2BGR_I420);

    cv::UMat crop = source(cv::Rect(m_params.crop.x,m_params.crop.y, m_params.crop.width, m_params.crop.height));

    cv::cvtColor(crop, crop, cv::COLOR_BGR2YUV_I420);

    cv::Mat dst_host;
	crop.copyTo(dst_host);

    MediaDataVideoBaseBand baseband_data;
    int crop_width = m_params.crop.width;
    int crop_height = m_params.crop.height;
	int y_size = crop_width * crop_height;
	int uv_size = y_size / 4;
	baseband_data.buffer[0] = dst_host.data;
	baseband_data.line_size[0] = crop_width;
	baseband_data.buffer[1] = dst_host.data + y_size;
	baseband_data.line_size[1] = crop_width / 2;
	baseband_data.buffer[2] = dst_host.data + y_size + uv_size;
	baseband_data.line_size[2] = crop_width / 2;
	baseband_data.fmt = VideoBaseBandFmt::YUV420P;
	baseband_data.width = crop_width;
	baseband_data.height = crop_height;
	baseband_data.pts = baseband->pts;
	m_cb(&baseband_data,m_user_data);
    return true;
}
bool CropFilter::Stop(){
    return true;
}