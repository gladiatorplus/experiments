#ifndef FRAMERATEFILTER_H_
#define FRAMERATEFILTER_H_

#include "../Media.h"

class FrameRateFilter : public Filter{
public:
	FrameRateFilter();
	virtual ~FrameRateFilter();
	virtual bool Start(MediaFilterParams & params);
	virtual int InputData(MediaData * data);
	virtual bool Stop();
private:
	struct AVFrame * m_output_frame = nullptr;
	struct AVFrame * m_input_frame = nullptr;
	struct AVFilterContext * m_buffersink_ctx = nullptr;
	struct AVFilterContext * m_buffersrc_ctx = nullptr;
	struct AVFilterGraph * m_filter_graph = nullptr;
	unsigned char * m_video_frame_buffer = nullptr;
	//int64_t m_frame_index = 0;
	double m_last_pts = -1;
};

#endif 
