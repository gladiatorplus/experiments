
#ifndef RATIOFILTER_H_
#define RATIOFILTER_H_

#include "../Media.h"

class RatioFilter: public Filter{
public:
	RatioFilter();
	virtual ~RatioFilter();
	virtual bool Start(MediaFilterParams & params);
	virtual int InputData(MediaData * data);
	virtual bool Stop();
private:
	MediaFilterParams m_params;
};

#endif 
