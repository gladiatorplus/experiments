#ifndef CROPFILTER_H
#define CROPFILTER_H

#include "../Media.h"
	
class CropFilter : public Filter{
public:
	CropFilter();
	~CropFilter();
	virtual bool Start(MediaFilterParams & params);
	virtual int InputData(MediaData * data);
	virtual bool Stop();
private:
	MediaFilterParams m_params;
};
#endif