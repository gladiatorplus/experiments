

#ifndef WATERMARKFILTER_H_
#define WATERMARKFILTER_H_

#include "../Media.h"

class WatermarkFilter : public Filter{
public:
	WatermarkFilter();
	virtual ~WatermarkFilter();
	virtual bool Start(MediaFilterParams & params);
	virtual int InputData(MediaData * data);
	virtual bool Stop();
private:
	struct AVFrame * m_output_frame = nullptr;
	struct AVFrame * m_input_frame = nullptr;
	struct AVFilterContext * m_buffersink_ctx = nullptr;
	struct AVFilterContext * m_buffersrc_ctx = nullptr;
	struct AVFilterGraph * m_filter_graph = nullptr;
	struct AVFilterInOut * m_outputs = nullptr; 
	struct AVFilterInOut * m_inputs = nullptr;
	unsigned char * m_video_frame_buffer = nullptr;
};

#endif 
