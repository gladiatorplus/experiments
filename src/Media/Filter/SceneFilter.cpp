
#include "include/cef_client.h"
#include "include/cef_frame.h"
#include "include/cef_string_visitor.h"
#include "include/cef_base.h"
#include "include/cef_app.h"

#include "SceneFilter.h"  
#include "opencv2/opencv.hpp"

///////////////////////////////////////////////////CEF////////////////////////////////////////////////////////////
class OffscreenBrowserApp : public CefApp, public CefBrowserProcessHandler, public CefRenderProcessHandler {
public:
	OffscreenBrowserApp(){}
	virtual ~OffscreenBrowserApp(){}
private:
	virtual CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() OVERRIDE{
		return this;
	}
	virtual CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() OVERRIDE{
		return this;
	}
	virtual void OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine> command_line) OVERRIDE{
		/*
		command_line->AppendSwitch(CefString("disable-gpu"));
		command_line->AppendSwitch(CefString("disable-gpu-compositing"));
		command_line->AppendSwitch(CefString("disable-gpu-vsync"));
		*/
	};
	IMPLEMENT_REFCOUNTING(OffscreenBrowserApp);
};

class OffScreenBrowserClient : public CefClient, public CefRenderHandler, public CefRequestHandler{
public:
	OffScreenBrowserClient(int width,int height) : m_width(width),m_height(height){};
	virtual ~OffScreenBrowserClient(){	}
	void SetDataCB(MediaDataCB cb,void * user_data){
		m_cb = cb;
		m_user_data = user_data;
	}
private:
	virtual void OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height) override {
		 if(m_cb){
			MediaDataVideoBaseBand baseband;
			baseband.fmt = VideoBaseBandFmt::BGRA;
			baseband.width = width;
			baseband.height = height;
			baseband.line_size[0] = width * 4;
			baseband.buffer[0] = (unsigned char*)buffer;
			baseband.pts = 0;
			m_cb(&baseband,m_user_data);
		}
	}
	virtual void GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect) override {
		rect.Set(0, 0, m_width, m_height);
	};
	virtual CefRefPtr<CefRenderHandler> GetRenderHandler() override {
		return this;
	}
	CefRefPtr<CefRequestHandler> GetRequestHandler() override {
		return this;
	}
	virtual bool OnCertificateError(CefRefPtr<CefBrowser> browser,
                                  cef_errorcode_t cert_error,
                                  const CefString& request_url,
                                  CefRefPtr<CefSSLInfo> ssl_info,
                                  CefRefPtr<CefRequestCallback> callback){
		if (cert_error == ERR_CERT_AUTHORITY_INVALID) {
			callback->Continue(true);
			return true;
		}else{
			return false;
		}
	}
	virtual void OnRenderProcessTerminated(CefRefPtr<CefBrowser> browser, TerminationStatus status) override {
		if(status != TS_PROCESS_CRASHED)
			browser->Reload();
	}
	IMPLEMENT_REFCOUNTING(OffScreenBrowserClient);
private:
	int m_width = 0;
	int m_height = 0;
	MediaDataCB m_cb = nullptr;
	void * m_user_data = nullptr;
};

struct CefStartTaskParams{
	std::string url;
	int screen_width = 0;
	int screen_height = 0;
	MediaDataCB cb = nullptr;
	void * user_data = nullptr;
};

class CefTaskStatus{
public:
	CefTaskStatus(){}
	virtual ~CefTaskStatus(){}
	bool IsTaskDone(){
		return m_task_done;
	}
protected:
	bool m_task_done = false;
};

class CefStartTask : public CefTask , public CefTaskStatus{
public:
	CefStartTask(){}
	virtual ~CefStartTask(){}
	void Init(CefStartTaskParams & params){
		m_params = params;
	}
	CefRefPtr<CefBrowser> GetCefBrowser(){
		return m_browser;
	}
private:
	CefStartTaskParams m_params;
	CefRefPtr<OffScreenBrowserClient> m_client = nullptr;
	CefRefPtr<CefBrowser> m_browser = nullptr;
private:
	void Execute() override {
		m_client = new OffScreenBrowserClient(m_params.screen_width,m_params.screen_height);
		m_client->SetDataCB(m_params.cb,m_params.user_data);
		CefWindowInfo info;
		info.SetAsWindowless(kNullWindowHandle);
		CefBrowserSettings browser_settings;
		m_browser = CefBrowserHost::CreateBrowserSync(info, m_client.get(), m_params.url, browser_settings, nullptr,nullptr);
		m_browser->GetMainFrame()->LoadURL(m_params.url);
		m_task_done = true;
	}
	IMPLEMENT_REFCOUNTING(CefStartTask);
};

class CefStopTask : public CefTask , public CefTaskStatus{
public:
	CefStopTask(CefRefPtr<CefBrowser> browser) : m_browser(browser) {}
	virtual ~CefStopTask(){}
private:
	CefRefPtr<CefBrowser> m_browser = nullptr;
private:
	void Execute() override {
		m_browser->StopLoad();
		m_browser->GetHost()->CloseBrowser(true);
		m_task_done = true;
	}
	IMPLEMENT_REFCOUNTING(CefStopTask);
};

class CefCloseTask : public CefTask , public CefTaskStatus{
public:
	CefCloseTask(){}
	virtual ~CefCloseTask(){}
private:
	void Execute() override {
		CefQuitMessageLoop();
		m_task_done = true;
	}
	IMPLEMENT_REFCOUNTING(CefCloseTask);
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define SCENE_RENDER_EXE_PATH "./SceneRender"

pthread_t SceneFilter::m_cef_message_thread = 0;

void SceneFilter::CloseCef(){
    if(m_cef_message_thread > 0){
		CefRefPtr<CefCloseTask> cef_close_task = new CefCloseTask();
		CefPostTask(TID_UI, cef_close_task.get());
		int retry_times = 0;
		while(!cef_close_task->IsTaskDone()){
			usleep(100 * 1000);
			if(retry_times++ >10)
				break;
		}
		pthread_join(m_cef_message_thread,nullptr);
		m_cef_message_thread = 0;
	}
}
	
SceneFilter::SceneFilter(){	
}
	
SceneFilter::~SceneFilter(){
    Stop();
}

bool SceneFilter::Start(MediaFilterParams & params){
    m_params = params;
	m_stop = false;
    if(!m_cef_frame.buffer[0]){
        m_cef_frame.buffer[0] = new unsigned char[m_params.scence.screen_width *  m_params.scence.screen_height * 4];
    }
	pthread_create(&m_working_thread, nullptr, &WorkingThreadFunc, this);
	return true;
}

static void AlphaBlending(cv::UMat & src,cv::UMat & src_with_alpha,cv::UMat & dst){
	std::vector<cv::UMat>src_chns;  
	std::vector<cv::UMat>src_with_alpha_chs; 
	std::vector<cv::UMat>dst_chns; 
	cv::split(src, src_chns);  
	cv::split(src_with_alpha, src_with_alpha_chs);
	cv::split(dst, dst_chns);
	cv::UMat alpha = src_with_alpha_chs[3];
	alpha.convertTo(alpha,CV_32FC1, 1/255.0);
	cv::UMat neg_alpha;
	cv::subtract(1,alpha,neg_alpha);
	for(int i=0;i<3;i++){
		cv::blendLinear(src_with_alpha_chs[i],src_chns[i],alpha,neg_alpha,dst_chns[i]);
	}
	cv::merge(dst_chns,dst);
}

int SceneFilter::InputData(MediaData * data){
    if(m_cef_frame.fmt == VideoBaseBandFmt::BGRA){
        AutoLock lock(&m_thread_lock);
        MediaDataVideoBaseBand * baseband = static_cast<MediaDataVideoBaseBand*>(data);
        cv::UMat source;
        if(baseband->fmt == VideoBaseBandFmt::YUV420P){
            cv::Mat souce_host(baseband->height * 3 / 2,baseband->width,CV_8UC1,baseband->buffer[0]);
            souce_host.copyTo(source);
        }else{
            return false;
        }

        cv::cvtColor(source, source, cv::COLOR_YUV2BGR_I420);

        cv::UMat cef_frame;
        cv::Mat cef_host(m_cef_frame.height,m_cef_frame.width,CV_8UC4,m_cef_frame.buffer[0]);
        cef_host.copyTo(cef_frame);

        cv::UMat out_frame(baseband->height,baseband->width,CV_8UC3);
		AlphaBlending(source,cef_frame,out_frame);

        cv::cvtColor(out_frame, out_frame, cv::COLOR_BGR2YUV_I420);

        cv::Mat out_frame_host;
        out_frame.copyTo(out_frame_host);

        MediaDataVideoBaseBand baseband_data;
        int y_size = baseband->width * baseband->height;
        int uv_size = y_size / 4;
        baseband_data.buffer[0] = out_frame_host.data;
        baseband_data.line_size[0] = baseband->width;
        baseband_data.buffer[1] = out_frame_host.data + y_size;
        baseband_data.line_size[1] = baseband->width / 2;
        baseband_data.buffer[2] = out_frame_host.data + y_size + uv_size;
        baseband_data.line_size[2] = baseband->width / 2;
        baseband_data.fmt = VideoBaseBandFmt::YUV420P;
        baseband_data.width = baseband->width;
        baseband_data.height = baseband->height;
        baseband_data.pts = baseband->pts;
        m_cb(&baseband_data,m_user_data);

    }else{
        m_cb(data,m_user_data);
    }
    return 0;
}
bool SceneFilter::Stop(){
	m_stop = true;
	if(m_working_thread > 0){
		pthread_join(m_working_thread,nullptr);
		m_working_thread = 0;
	}
	if(m_cef_frame.buffer[0]){
		delete [] m_cef_frame.buffer[0];
		m_cef_frame.buffer[0] = nullptr;
	}
	return true;
}

void * SceneFilter::CefMessageThreadFunc(void * arg){
	if(!arg)
		return nullptr;
	SceneFilter * obj = (SceneFilter*)arg;
	CefMainArgs main_args(g_config.app_args.argc, g_config.app_args.argv);
	CefSettings settings;
	settings.windowless_rendering_enabled = true;
	settings.no_sandbox = true;
	settings.multi_threaded_message_loop = false;
	CefString(&settings.browser_subprocess_path).FromASCII(SCENE_RENDER_EXE_PATH);
	CefRefPtr<OffscreenBrowserApp> app = new OffscreenBrowserApp();
	CefInitialize(main_args, settings, app, nullptr);
	SemPost(&obj->m_cef_inited_sem);
	CefRunMessageLoop();
	CefShutdown();
	return nullptr;
}

void SceneFilter::CefFrameCB(MediaData *data, void * user_data){
    SceneFilter * obj = (SceneFilter*)user_data;
    AutoLock lock(&obj->m_thread_lock);
    MediaDataVideoBaseBand * baseband = static_cast<MediaDataVideoBaseBand*>(data);
    obj->m_cef_frame.fmt = baseband->fmt;
    obj->m_cef_frame.width = baseband->width;
    obj->m_cef_frame.height = baseband->height;
    obj->m_cef_frame.line_size[0] = baseband->line_size[0];
    memcpy(obj->m_cef_frame.buffer[0],baseband->buffer[0],baseband->line_size[0] * baseband->height);
}

void * SceneFilter::WorkingThreadFunc(void *arg){
	SceneFilter * obj = (SceneFilter*)arg;
	obj->StartPull();
	return nullptr;
}
bool SceneFilter::StartPull(){
    if(m_cef_message_thread == 0){
        SemInit(&m_cef_inited_sem,0);
        pthread_create(&m_cef_message_thread, nullptr, &CefMessageThreadFunc, this);
        if(SemTimedWait(&m_cef_inited_sem,5000) < 0){
            SLOG_ERROR<<"SceneFilter::StartPull, can not start cef message thread";
            return false;
        }
    }

    CefRefPtr<CefStartTask> cef_start_task = new CefStartTask();
    CefStartTaskParams cef_param;
    cef_param.cb = CefFrameCB;
    cef_param.user_data = this;
    cef_param.url = m_params.scence.url;
    cef_param.screen_height = m_params.scence.screen_height;
    cef_param.screen_width = m_params.scence.screen_width;
    cef_start_task->Init(cef_param);
    CefPostTask(TID_UI, cef_start_task.get());
    int retry_times = 0;
    while(!cef_start_task->IsTaskDone()){
        usleep(100 * 1000);
        if(retry_times++ > 10)
            break;
    }

    if(!cef_start_task->IsTaskDone()){
        SLOG_ERROR<<"SceneFilter::StartPull, can not start cef render task";
        return false;
    }

    while(!m_stop){
        usleep(500 * 1000);
    }

    CefRefPtr<CefStopTask> cef_stop_task = new CefStopTask(cef_start_task->GetCefBrowser());
    CefPostTask(TID_UI, cef_stop_task.get());
    retry_times = 0;
    while(!cef_stop_task->IsTaskDone()){
        usleep(100 * 1000);
        if(retry_times++ > 10)
            break;
    }

    if(!cef_stop_task->IsTaskDone()){
        SLOG_ERROR<<"SceneFilter::StartPull, can not stop cef render task";
        return false;
    }
	
	return true;
}