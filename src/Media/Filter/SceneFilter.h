#ifndef SCENEFILTER_H
#define SCENEFILTER_H

#include "../Media.h"
	
class SceneFilter : public Filter{
public:
	SceneFilter();
	~SceneFilter();
	bool Start(MediaFilterParams & params);
	int InputData(MediaData * data);
	bool Stop();
private:
	static pthread_t m_cef_message_thread;
	static void * CefMessageThreadFunc(void * arg);
	static void CloseCef();
private:
    static void CefFrameCB(MediaData *data, void * user_data);
	static void * WorkingThreadFunc(void *arg);
	bool StartPull();
private:
	pthread_t m_working_thread = 0;
	sem_t m_cef_inited_sem;
	bool m_stop = false;
	MediaFilterParams m_params;
	MediaDataVideoBaseBand m_cef_frame;
	pthread_mutex_t m_thread_lock = PTHREAD_MUTEX_INITIALIZER;
};
#endif