#include "RotateFilter.h"  
	
#include "libyuv.h"

RotateFilter::RotateFilter(){	
}
	
RotateFilter::~RotateFilter(){	
    Stop();
}

bool RotateFilter::Start(MediaFilterParams & params){
    if(params.rotate.angle != 90 && params.rotate.angle != 180 && params.rotate.angle != 270)
        return false;
    m_params = params;
    return true;
}

int RotateFilter::InputData(MediaData * data){
    MediaDataVideoBaseBand * baseband = static_cast<MediaDataVideoBaseBand*>(data);
    MediaDataVideoBaseBand rotate_baseband = *baseband;
    libyuv::RotationModeEnum rotate_mode = libyuv::kRotateNone;
    switch (m_params.rotate.angle){
        case 90:
            rotate_mode = libyuv::kRotate90;
            rotate_baseband.width = baseband->height;
            rotate_baseband.height = baseband->width;
            rotate_baseband.line_size[0] = rotate_baseband.width;
            rotate_baseband.line_size[1] = rotate_baseband.width >> 1;
            rotate_baseband.line_size[2] = rotate_baseband.width >> 1;
            break;
        case 180:
            rotate_mode = libyuv::kRotate180;
            break;
        case 270:
            rotate_mode = libyuv::kRotate270;
            rotate_baseband.width = baseband->height;
            rotate_baseband.height = baseband->width;
            rotate_baseband.line_size[0] = rotate_baseband.width;
            rotate_baseband.line_size[1] = rotate_baseband.width >> 1;
            rotate_baseband.line_size[2] = rotate_baseband.width >> 1;
            break;
        default:
            break;
    }

    if(rotate_mode > libyuv::kRotateNone){
        if(!m_rotate_buffer){
            m_rotate_buffer = new unsigned char[rotate_baseband.width * rotate_baseband.height * 3 / 2];
        }
        int dst_u_index = baseband->width * baseband->height;
        int dst_v_index = dst_u_index + baseband->width * baseband->height / 4;
        rotate_baseband.buffer[0] = m_rotate_buffer;
        rotate_baseband.buffer[1] = m_rotate_buffer + dst_u_index;
        rotate_baseband.buffer[2] = m_rotate_buffer + dst_v_index;
        libyuv::I420Rotate(baseband->buffer[0],baseband->line_size[0],
                baseband->buffer[1],baseband->line_size[1],
                baseband->buffer[2],baseband->line_size[2],
                rotate_baseband.buffer[0],rotate_baseband.line_size[0],
                rotate_baseband.buffer[1],rotate_baseband.line_size[1],
                rotate_baseband.buffer[2],rotate_baseband.line_size[2],
                baseband->width,baseband->height,rotate_mode);
        m_cb(&rotate_baseband,m_user_data);
    }else{
        m_cb(baseband,m_user_data);
    }
    return 0;
}
bool RotateFilter::Stop(){
    if(m_rotate_buffer){
        delete [] m_rotate_buffer;
        m_rotate_buffer = nullptr;
    }
    return true;
}