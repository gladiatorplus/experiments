#ifndef ROTATEFILTER_H
#define ROTATEFILTER_H

#include "../Media.h"

class RotateFilter : public Filter {
public:
	RotateFilter();
	~RotateFilter();
	virtual bool Start(MediaFilterParams & params);
	virtual int InputData(MediaData * data);
	virtual bool Stop();
private:
	MediaFilterParams m_params;
	unsigned char * m_rotate_buffer = nullptr;
};
#endif