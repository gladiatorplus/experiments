
#include <sys/sysinfo.h>

#include "FFVideoDecoder.h"

extern "C"{
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/opt.h>
}

FFVideoDecoder::FFVideoDecoder() {
}

FFVideoDecoder::~FFVideoDecoder() {
	Stop();
}

bool FFVideoDecoder::Start(MediaParams & info){
	Stop();
	AVCodecID video_codec_id = AV_CODEC_ID_NONE;
	AVCodec * video_codec = nullptr;
	switch(info.video.codec){
		case VideoCodec::H264:
			video_codec = avcodec_find_decoder(AV_CODEC_ID_H264);
			video_codec_id = AV_CODEC_ID_H264;
			break;
		case VideoCodec::HEVC:
			video_codec = avcodec_find_decoder(AV_CODEC_ID_HEVC);
			video_codec_id = AV_CODEC_ID_HEVC;
			break;
		case VideoCodec::VP9:
			video_codec = avcodec_find_decoder(AV_CODEC_ID_VP9);
			video_codec_id = AV_CODEC_ID_VP9;
			break;
		default:
			break;
	}

	if(video_codec){
		m_video_codec_ctx = avcodec_alloc_context3(video_codec);
		if(!m_video_codec_ctx)
			return false;
		int cpus = get_nprocs();
		m_video_codec_ctx->thread_count = cpus == 0 ? 4 : cpus;
		if (avcodec_open2(m_video_codec_ctx, video_codec, nullptr) < 0) {
			return false;
		}
		m_video_parser = av_parser_init(video_codec_id);
		if(!m_video_parser)
			return false;
		m_video_frame = av_frame_alloc();
		if(!m_video_frame)
			return false;
		m_video_es_buffer = new unsigned char[1024*1024*20];
		memset(m_video_es_buffer,0,1024*1024*20);
		m_current_video_es_buffer_size = 0;
	}else{
		if(info.video.codec > VideoCodec::NONE)
			return false;
	}
	return true;
}
int FFVideoDecoder::InputData(MediaData * data){

	MediaDataBitStream * bs = static_cast<MediaDataBitStream*>(data);

	if(bs->media_type == MediaType::VIDEO){
		if(!m_video_codec_ctx || !m_video_parser || !m_video_es_buffer || !m_video_frame)
			return -1;
		memcpy(m_video_es_buffer + m_current_video_es_buffer_size,bs->buffer,bs->buffer_len);
		m_current_video_es_buffer_size += bs->buffer_len;
		while(m_current_video_es_buffer_size){
			AVPacket pkt={};
			int consumed = av_parser_parse2(m_video_parser,m_video_codec_ctx,&pkt.data,&pkt.size,
					m_video_es_buffer, m_current_video_es_buffer_size,bs->pts, bs->dts, 0);
			if(consumed < 0)
				return -1;

			if(pkt.data && pkt.size){
				bs->is_key ? pkt.flags |= AV_PKT_FLAG_KEY : pkt.flags;
				pkt.pts = m_video_parser->pts > 0 ? m_video_parser->pts : bs->pts;
				pkt.dts = m_video_parser->dts > 0 ? m_video_parser->dts : bs->dts;
				int got_picture = 0;
				avcodec_decode_video2(m_video_codec_ctx, m_video_frame, &got_picture, &pkt);
				m_current_video_es_buffer_size -= consumed;
				memmove(m_video_es_buffer,m_video_es_buffer + consumed, m_current_video_es_buffer_size);
				av_free_packet(&pkt);
				if (got_picture){
					OutputVideoFrame();
				}
			}else{
				m_current_video_es_buffer_size -= consumed;
				memmove(m_video_es_buffer,m_video_es_buffer + consumed, m_current_video_es_buffer_size);
			}
		}
	}else
		return -1;

	return 0;
}
bool FFVideoDecoder::Stop(){
	FLushVideoDecoder();
	if (m_video_codec_ctx) {
		avcodec_close(m_video_codec_ctx);
		avcodec_free_context(&m_video_codec_ctx);
		m_video_codec_ctx = nullptr;
	}
	if(m_video_parser){
		av_parser_close(m_video_parser);
		m_video_parser = nullptr;
	}
	if(m_video_es_buffer){
		delete [] m_video_es_buffer;
		m_video_es_buffer = nullptr;
		m_current_video_es_buffer_size = 0;
	}
	if(m_video_frame){
		av_frame_free(&m_video_frame);
		m_video_frame = nullptr;
	}
	if(m_transformed_video_frame){
		avpicture_free((AVPicture*) m_transformed_video_frame);
		av_frame_free(&m_transformed_video_frame);
		m_transformed_video_frame = nullptr;
	}

	if(m_img_convert_ctx){
		sws_freeContext(m_img_convert_ctx);
		m_img_convert_ctx = nullptr;
	}

	if(m_video_frame_buffer){
		delete []m_video_frame_buffer;
		m_video_frame_buffer = nullptr;
	}
	return true;
}

bool FFVideoDecoder::OutputVideoFrame(){
	if(!m_cb || !m_video_frame || !m_video_codec_ctx)
		return false;

	AVFrame * output_frame = nullptr;
	m_video_frame->pts = av_frame_get_best_effort_timestamp(m_video_frame);
	if(m_video_frame->format != AV_PIX_FMT_YUV420P){
		if(!m_img_convert_ctx){
			m_img_convert_ctx = sws_getContext(m_video_codec_ctx->width, m_video_codec_ctx->height, m_video_codec_ctx->pix_fmt,
					m_video_codec_ctx->width, m_video_codec_ctx->height,
					AV_PIX_FMT_YUV420P, SWS_FAST_BILINEAR, nullptr, nullptr, nullptr);

			if(!m_img_convert_ctx)
				return false;
		}
		if(!m_transformed_video_frame){
			m_transformed_video_frame = av_frame_alloc();
			if (!m_transformed_video_frame) {
				return false;
			}
			avpicture_alloc((AVPicture*)m_transformed_video_frame, AV_PIX_FMT_YUV420P,m_video_codec_ctx->width,m_video_codec_ctx->height);
			m_transformed_video_frame->width = m_video_frame->width;
			m_transformed_video_frame->height = m_video_frame->height;
			m_transformed_video_frame->format = AV_PIX_FMT_YUV420P;
		}
		m_transformed_video_frame->pts = m_video_frame->pts;
		if(sws_scale(m_img_convert_ctx, (const unsigned char * const *)m_video_frame->data,
				m_video_frame->linesize, 0, m_video_frame->height, m_transformed_video_frame->data,
				m_transformed_video_frame->linesize) < 0){
			return false;
		}
		output_frame = m_transformed_video_frame;
	}else{
		output_frame = m_video_frame;
	}

	if(!m_video_frame_buffer)
		m_video_frame_buffer = new unsigned char[7680*4320*2];

	int source_y = 0,source_u = 0,source_v =0;
	int dst_y = 0,dst_u = output_frame->width * output_frame->height,dst_v = output_frame->width * output_frame->height * 5 / 4;
	for(int i=0;i<output_frame->height;i++){
		memcpy(m_video_frame_buffer + dst_y,output_frame->data[0] + source_y,output_frame->width);
		dst_y += output_frame->width;
		source_y += output_frame->linesize[0];
		if(i < output_frame->height / 2){
			memcpy(m_video_frame_buffer + dst_u,output_frame->data[1] + source_u,output_frame->width / 2);
			memcpy(m_video_frame_buffer + dst_v,output_frame->data[2] + source_v,output_frame->width / 2);
			dst_u += output_frame->width / 2;
			dst_v += output_frame->width / 2;
			source_u += output_frame->linesize[1];
			source_v += output_frame->linesize[2];
		}
	}

	MediaDataVideoBaseBand baseband_data;
	baseband_data.buffer[0] = m_video_frame_buffer;
	baseband_data.line_size[0] = output_frame->width;
	baseband_data.buffer[1] = m_video_frame_buffer + output_frame->width * output_frame->height;
	baseband_data.line_size[1] = output_frame->width / 2;
	baseband_data.buffer[2] = m_video_frame_buffer + output_frame->width * output_frame->height * 5 / 4;
	baseband_data.line_size[2] = output_frame->width / 2;
	baseband_data.fmt = VideoBaseBandFmt::YUV420P;
	baseband_data.width = output_frame->width;
	baseband_data.height = output_frame->height;
	baseband_data.pts = output_frame->pts;
	m_cb(&baseband_data,m_user_data);

	return true;
}
bool FFVideoDecoder::FLushVideoDecoder(){
	if(!m_video_codec_ctx || !m_video_parser || !m_video_es_buffer || !m_video_frame)
		return false;
	AVPacket pkt={};
	av_parser_parse2(m_video_parser,m_video_codec_ctx,&pkt.data,&pkt.size,
			m_video_es_buffer, 0, AV_NOPTS_VALUE,  AV_NOPTS_VALUE, 0);

	int got_picture = 0;
	if(pkt.data && pkt.size){
		pkt.pts = m_video_parser->pts;
		pkt.dts = m_video_parser->dts;
		avcodec_decode_video2(m_video_codec_ctx, m_video_frame, &got_picture, &pkt);
		av_free_packet(&pkt);
		if (got_picture){
			OutputVideoFrame();
		}

	}
	pkt = {};
	pkt.data = nullptr;
	pkt.size = 0;
	pkt.pts = 0;
	pkt.dts = 0;
	avcodec_decode_video2(m_video_codec_ctx, m_video_frame, &got_picture, &pkt);
	av_free_packet(&pkt);
	if (got_picture){
		OutputVideoFrame();
	}
	return true;
}
