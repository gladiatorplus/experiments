#ifndef FFVIDEODECODER_H_
#define FFVIDEODECODER_H_

#include "../../Media.h"

class FFVideoDecoder : public Decoder {
public:
	FFVideoDecoder();
	virtual ~FFVideoDecoder();
	bool Start(MediaParams & info);
	virtual int InputData(MediaData * data);
    virtual bool Stop();
private:
	struct AVCodecContext  * m_video_codec_ctx = nullptr;
	struct AVCodecParserContext   * m_video_parser = nullptr;
	struct AVFrame 		   * m_video_frame = nullptr;
	struct AVFrame 		   * m_transformed_video_frame = nullptr;
	struct SwsContext      * m_img_convert_ctx = nullptr;
	unsigned char 		   * m_video_frame_buffer = nullptr;
	unsigned char		   * m_video_es_buffer = nullptr;
	int						 m_current_video_es_buffer_size = 0;
private:
	bool OutputVideoFrame();
	bool FLushVideoDecoder();
};

#endif 
