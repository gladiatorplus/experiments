
#include "FFAudioDecoder.h"

extern "C"{
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/opt.h>
}

FFAudioDecoder::FFAudioDecoder() {
}

FFAudioDecoder::~FFAudioDecoder() {
	Stop();
}


bool FFAudioDecoder::Start(MediaParams & info){
	Stop();
	AVCodecID audio_codec_id = AV_CODEC_ID_NONE;
	AVCodec * audio_codec = nullptr;
	switch(info.audio.codec){
		case AudioCodec::AAC:
			audio_codec = avcodec_find_decoder(AV_CODEC_ID_AAC);
			audio_codec_id = AV_CODEC_ID_AAC;
			break;
		case AudioCodec::MP3:
			audio_codec = avcodec_find_decoder(AV_CODEC_ID_MP3);
			audio_codec_id = AV_CODEC_ID_MP3;
			break;
		case AudioCodec::OPUS:
			audio_codec = avcodec_find_decoder(AV_CODEC_ID_OPUS);
			audio_codec_id = AV_CODEC_ID_OPUS;
			break;
		default:
			break;
	}

	if(audio_codec){
		m_audio_codec_ctx = avcodec_alloc_context3(audio_codec);
		if(!m_audio_codec_ctx)
			return false;

		if (avcodec_open2(m_audio_codec_ctx, audio_codec, nullptr) < 0) {
			return false;
		}
		m_audio_parser = av_parser_init(audio_codec_id);
		if(!m_audio_parser)
			return false;
		m_audio_frame = av_frame_alloc();
		if(!m_audio_frame)
			return false;
		m_audio_es_buffer = new unsigned char[1024*1024*10];
		memset(m_audio_es_buffer,0,1024*1024*10);
		m_current_audio_es_buffer_size = 0;
	}else{
		if(info.audio.codec > AudioCodec::NONE)
			return false;
	}
	return true;
}
int FFAudioDecoder::InputData(MediaData * data){
	MediaDataBitStream * bs = static_cast<MediaDataBitStream*>(data);

	if(bs->media_type == MediaType::AUDIO){
		if(!m_audio_codec_ctx || !m_audio_parser || !m_audio_es_buffer || !m_audio_frame)
			return -1;
		memcpy(m_audio_es_buffer + m_current_audio_es_buffer_size,bs->buffer,bs->buffer_len);
		m_current_audio_es_buffer_size += bs->buffer_len;
		while(m_current_audio_es_buffer_size){
			AVPacket pkt={};
			int consumed = av_parser_parse2(m_audio_parser,m_audio_codec_ctx,&pkt.data,&pkt.size,
					m_audio_es_buffer, m_current_audio_es_buffer_size,bs->pts, bs->dts, 0);
			if(consumed < 0)
				return -1;
			if(pkt.data && pkt.size){
				pkt.pts = m_audio_parser->pts > 0 ? m_audio_parser->pts : bs->pts;
				pkt.dts = m_audio_parser->dts > 0 ? m_audio_parser->dts : bs->dts;
				int got_picture = 0;
				avcodec_decode_audio4(m_audio_codec_ctx, m_audio_frame, &got_picture, &pkt);
				m_current_audio_es_buffer_size -= consumed;
				memmove(m_audio_es_buffer,m_audio_es_buffer + consumed, m_current_audio_es_buffer_size);
				av_free_packet(&pkt);
				if (got_picture){
					m_audio_frame->pts = m_audio_frame->pkt_pts;
					OutputAudioFrame();
				}
			}else{
				m_current_audio_es_buffer_size -= consumed;
				memmove(m_audio_es_buffer,m_audio_es_buffer + consumed, m_current_audio_es_buffer_size);
			}
		}
	}else
		return -1;
	return 0;
}
bool FFAudioDecoder::Stop(){
	FlushAudioDecoder();
	if (m_audio_codec_ctx) {
		avcodec_close(m_audio_codec_ctx);
		avcodec_free_context(&m_audio_codec_ctx);
		m_audio_codec_ctx = nullptr;
	}
	if(m_audio_parser){
		av_parser_close(m_audio_parser);
		m_audio_parser = nullptr;
	}
	if(m_audio_es_buffer){
		delete [] m_audio_es_buffer;
		m_audio_es_buffer = nullptr;
		m_current_audio_es_buffer_size = 0;
	}
	if(m_audio_frame){
		av_frame_free(&m_audio_frame);
		m_audio_frame = nullptr;
	}
	if (m_audio_convert_ctx) {
		swr_free(&m_audio_convert_ctx);
		m_audio_convert_ctx = nullptr;
	}

	if(m_audio_out_pcm_buffer){
		delete [] m_audio_out_pcm_buffer;
		m_audio_out_pcm_buffer = nullptr;
	}
	return true;
}
bool FFAudioDecoder::OutputAudioFrame(){
	if(!m_cb || !m_audio_frame || !m_audio_codec_ctx)
		return false;

	if(!m_audio_out_pcm_buffer)
		m_audio_out_pcm_buffer = new unsigned char[1024*1024*10];
	int out_pcm_buffer_size = 0;

	AVSampleFormat dst_sample_fmt = AV_SAMPLE_FMT_S16;

	if(m_audio_frame->format != dst_sample_fmt ||
	    m_audio_frame->channels != m_audio_codec_ctx->channels ||
		m_audio_frame->sample_rate != m_audio_codec_ctx->sample_rate){
		if (!m_audio_convert_ctx) {
			m_audio_convert_ctx = swr_alloc_set_opts(m_audio_convert_ctx,
					av_get_default_channel_layout(m_audio_codec_ctx->channels), dst_sample_fmt, m_audio_codec_ctx->sample_rate,
					m_audio_frame->channel_layout,(AVSampleFormat)m_audio_frame->format, m_audio_frame->sample_rate,
					0, 0);
			if (!m_audio_convert_ctx)
				return false;
			if (swr_init(m_audio_convert_ctx) < 0)
				return false;
		}
		int out_samples = swr_get_out_samples(m_audio_convert_ctx, m_audio_frame->nb_samples);
		int dst_samples = swr_convert(m_audio_convert_ctx, &m_audio_out_pcm_buffer, out_samples, (const uint8_t**)&m_audio_frame->data, m_audio_frame->nb_samples);
		out_pcm_buffer_size = av_samples_get_buffer_size(nullptr, m_audio_codec_ctx->channels,dst_samples, dst_sample_fmt, 1);
	}else{
		int dst_bufsize = av_samples_get_buffer_size(m_audio_frame->linesize, m_audio_frame->channels,m_audio_frame->nb_samples, dst_sample_fmt, 1);
		memcpy(m_audio_out_pcm_buffer,m_audio_frame->extended_data[0],dst_bufsize);
		out_pcm_buffer_size = dst_bufsize;
	}

	MediaDataAudioBaseBand baseband_data;
	baseband_data.buffer = m_audio_out_pcm_buffer;
	baseband_data.buffer_len = out_pcm_buffer_size;
	baseband_data.fmt = AudioBaseBandFmt::S16;
	baseband_data.channels = m_audio_codec_ctx->channels;
	baseband_data.sample_rate = m_audio_codec_ctx->sample_rate;
	baseband_data.pts = m_audio_frame->pts;
	m_cb(&baseband_data,m_user_data);

	return true;
}
bool FFAudioDecoder::FlushAudioDecoder(){
	if(!m_audio_codec_ctx || !m_audio_parser || !m_audio_es_buffer || !m_audio_frame)
		return false;
	AVPacket pkt={};
	av_parser_parse2(m_audio_parser,m_audio_codec_ctx,&pkt.data,&pkt.size,
	  m_audio_es_buffer, 0, AV_NOPTS_VALUE,  AV_NOPTS_VALUE, 0);

	int got_picture = 0;
	if(pkt.data && pkt.size){
		pkt.pts = m_audio_parser->pts;
		pkt.dts = m_audio_parser->dts;
		avcodec_decode_audio4(m_audio_codec_ctx, m_audio_frame, &got_picture, &pkt);
		av_free_packet(&pkt);
		if (got_picture){
			  m_audio_frame->pts = m_audio_frame->pkt_pts;
			  OutputAudioFrame();
		}
	}
	pkt = {};
	pkt.data = nullptr;
	pkt.size = 0;
	pkt.pts = 0;
	pkt.dts = 0;
	avcodec_decode_video2(m_audio_codec_ctx, m_audio_frame, &got_picture, &pkt);
	av_free_packet(&pkt);
	if (got_picture){
		m_audio_frame->pts = m_audio_frame->pkt_pts;
		OutputAudioFrame();
	}
	return true;
}
