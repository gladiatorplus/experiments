#ifndef FFAUDIODECODER_H_
#define FFAUDIODECODER_H_

#include "../../Media.h"
class FFAudioDecoder : public Decoder {
public:
	FFAudioDecoder();
	virtual ~FFAudioDecoder();
	virtual bool Start(MediaParams & info);
	virtual int InputData(MediaData * data);
	virtual bool Stop();
private:
	struct AVCodecContext  * m_audio_codec_ctx = nullptr;
	struct AVCodecParserContext   * m_audio_parser = nullptr;
	struct AVFrame 		   * m_audio_frame = nullptr;
	unsigned char		   * m_audio_es_buffer = nullptr;
	int						 m_current_audio_es_buffer_size = 0;
	struct SwrContext 		* m_audio_convert_ctx = nullptr;
	unsigned char 			* m_audio_out_pcm_buffer = nullptr;
private:
	bool OutputAudioFrame();
	bool FlushAudioDecoder();
};

#endif 
