

#include "FFAudioEncoder.h"

extern "C"{
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/audio_fifo.h>
}


FFAudioEncoder::FFAudioEncoder() {
}

FFAudioEncoder::~FFAudioEncoder() {
	Stop();
}

bool FFAudioEncoder::Start(MediaParams & params){
	if(params.audio.codec > AudioCodec::NONE){
		AVCodecID codec_id = AV_CODEC_ID_NONE;
		switch (params.audio.codec) {
			case AudioCodec::AAC: {
				codec_id = AV_CODEC_ID_AAC;
				break;
			}
			case AudioCodec::MP3: {
				codec_id = AV_CODEC_ID_MP3;
				break;
			}
			default:
				return false;
		}

		struct AVCodec  * avcodec = avcodec_find_encoder(codec_id);

		if (avcodec == nullptr) {
			return false;
		}

		m_audio_codec_ctx = avcodec_alloc_context3(avcodec);
		if (m_audio_codec_ctx == nullptr) {
			return false;
		}
		m_audio_codec_ctx->sample_fmt = AV_SAMPLE_FMT_S16;
		m_audio_codec_ctx->sample_rate = params.audio.sample_rate;
		m_audio_codec_ctx->channels = params.audio.channels;
		m_audio_codec_ctx->bit_rate = params.audio.bit_rate;
		m_audio_codec_ctx->channel_layout = av_get_default_channel_layout(params.audio.channels);
		m_audio_codec_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
		if (avcodec_open2(m_audio_codec_ctx, avcodec, nullptr) < 0) {
			avcodec_free_context(&m_audio_codec_ctx);
			return false;
		}
	}
	return true;
}
bool FFAudioEncoder::InputData(MediaData * data){
	if(data->media_data_type == MediaDataType::BASEBAND && data->media_type == MediaType::AUDIO){
		MediaDataAudioBaseBand * baseband = static_cast<MediaDataAudioBaseBand *>(data);
		return EncodeAudio(baseband);
	}else
		return false;

	return true;
}


bool FFAudioEncoder::FLushEncoder(){
	if(m_audio_codec_ctx){
		AVPacket pkt;
		pkt.data = nullptr;
		pkt.size = 0;
		av_init_packet(&pkt);

		if (m_audio_fifo && m_resample_audio_frame) {
			int remain_pcm_size = av_audio_fifo_size(m_audio_fifo);
			int got_picture = 0;
			av_audio_fifo_read(m_audio_fifo, (void **)m_resample_audio_frame->data, remain_pcm_size);
			if (avcodec_encode_audio2(m_audio_codec_ctx, &pkt, m_resample_audio_frame, &got_picture) < 0) {
				av_free_packet(&pkt);
				return false;
			}
			if (got_picture) {
				OutputAudioData(pkt);
				av_free_packet(&pkt);
			}
		}


		int got_picture = 0;
		do {
			pkt.data = nullptr;
			pkt.size = 0;
			av_init_packet(&pkt);
			if (avcodec_encode_audio2(m_audio_codec_ctx, &pkt, nullptr, &got_picture) < 0) {
				av_free_packet(&pkt);
				return false;
			}
			if (got_picture) {
				OutputAudioData(pkt);
				av_free_packet(&pkt);
			}

		} while (got_picture);
	}
	return true;
}

bool FFAudioEncoder::Stop(){
	FLushEncoder();
	if (m_audio_codec_ctx) {
		avcodec_close(m_audio_codec_ctx);
		avcodec_free_context(&m_audio_codec_ctx);
		m_audio_codec_ctx = nullptr;
	}
	if (m_audio_frame) {
		av_frame_free(&m_audio_frame);
		m_audio_frame = nullptr;
	}
	if (m_resample_audio_frame) {
		av_frame_free(&m_resample_audio_frame);
		m_resample_audio_frame = nullptr;
	}
	if (m_audio_convert_ctx) {
		swr_free(&m_audio_convert_ctx);
		m_audio_convert_ctx = nullptr;
	}
	if (m_audio_fifo) {
		av_audio_fifo_free(m_audio_fifo);
		m_audio_fifo = nullptr;
	}
	if (m_audio_pcm_buffer) {
		av_free(m_audio_pcm_buffer[0]);
		av_free(m_audio_pcm_buffer);
		m_audio_pcm_buffer = nullptr;
	}
	if(m_audio_output_buffer){
		delete [] m_audio_output_buffer;
		m_audio_output_buffer = nullptr;
	}
	return true;
}
bool FFAudioEncoder::EncodeAudio(MediaDataAudioBaseBand * baseband){
	if (!m_audio_codec_ctx || !baseband)
		return false;

	AVPacket pkt;
	pkt.data = nullptr;
	pkt.size = 0;
	av_init_packet(&pkt);
	int got_picture = 0;
	if (!m_audio_frame) {
		m_audio_frame = av_frame_alloc();
	}

	m_audio_frame->pts = baseband->pts;
	m_audio_frame->channels = baseband->channels;
	m_audio_frame->channel_layout = av_get_default_channel_layout(m_audio_frame->channels);
	m_audio_frame->sample_rate = baseband->sample_rate;
	switch (baseband->fmt) {
		case AudioBaseBandFmt::S16 : {
			m_audio_frame->format = AV_SAMPLE_FMT_S16;
			m_audio_frame->nb_samples = baseband->buffer_len / baseband->channels / 2;
			avcodec_fill_audio_frame(m_audio_frame, m_audio_frame->channels, AV_SAMPLE_FMT_S16, (const uint8_t*)baseband->buffer, baseband->buffer_len, 1);
			break;
		}
		default:
			return false;
	}

	if (m_audio_frame->channels != m_audio_codec_ctx->channels ||
		m_audio_frame->sample_rate != m_audio_codec_ctx->sample_rate ||
		m_audio_frame->format != m_audio_codec_ctx->sample_fmt) {
		ResamleAudio(m_audio_frame);
		while (av_audio_fifo_size(m_audio_fifo) >= m_audio_codec_ctx->frame_size) {
			if (!m_resample_audio_frame) {
				m_resample_audio_frame = av_frame_alloc();
				m_resample_audio_frame->format = m_audio_codec_ctx->sample_fmt;
				m_resample_audio_frame->channels = m_audio_codec_ctx->channels;
				m_resample_audio_frame->channel_layout = m_audio_codec_ctx->channel_layout;
				m_resample_audio_frame->nb_samples = m_audio_codec_ctx->frame_size;
				m_resample_audio_frame->pts = 0;
				m_resample_audio_frame->pkt_pts = 0;
				if (av_frame_get_buffer(m_resample_audio_frame, 0) < 0)
					return -1;
			}
			m_resample_audio_frame->pts = m_audio_frame->pts;
			av_audio_fifo_read(m_audio_fifo, (void **)m_resample_audio_frame->data, m_audio_codec_ctx->frame_size);
			if (avcodec_encode_audio2(m_audio_codec_ctx, &pkt, m_resample_audio_frame, &got_picture) < 0) {
				av_free_packet(&pkt);
				return false;
			}
			if (got_picture) {
				OutputAudioData(pkt);
				av_free_packet(&pkt);
			}
		}
	} else {
		if (avcodec_encode_audio2(m_audio_codec_ctx, &pkt, m_audio_frame, &got_picture) < 0) {
			av_free_packet(&pkt);
			return false;
		}
		if (got_picture) {
			OutputAudioData(pkt);
			av_free_packet(&pkt);
		}
	}

	return true;
}
bool FFAudioEncoder::ResamleAudio(AVFrame * src_frame){
	if (!m_audio_codec_ctx)
		return false;
	if (!m_audio_convert_ctx) {
		m_audio_convert_ctx = swr_alloc_set_opts(m_audio_convert_ctx,
			m_audio_codec_ctx->channel_layout, m_audio_codec_ctx->sample_fmt, m_audio_codec_ctx->sample_rate,
			src_frame->channel_layout, (AVSampleFormat)src_frame->format, src_frame->sample_rate,
			0, 0);
		if (!m_audio_convert_ctx)
			return false;
		if (swr_init(m_audio_convert_ctx) < 0)
			return false;
	}

	if (!m_audio_fifo)
		m_audio_fifo = av_audio_fifo_alloc(m_audio_codec_ctx->sample_fmt, m_audio_codec_ctx->channels, 1);
	if (!m_audio_fifo)
		return false;

	int linesize = 0;
	if (!m_audio_pcm_buffer) {
		m_max_dst_nb_samples = av_rescale_rnd(src_frame->nb_samples, m_audio_codec_ctx->sample_rate, src_frame->sample_rate, AV_ROUND_UP);

		av_samples_alloc_array_and_samples(&m_audio_pcm_buffer, &linesize, m_audio_codec_ctx->channels, m_max_dst_nb_samples, m_audio_codec_ctx->sample_fmt, 0);
	}
	if (!m_audio_pcm_buffer)
		return false;

	int dst_nb_samples = av_rescale_rnd(swr_get_delay(m_audio_convert_ctx, src_frame->sample_rate) + src_frame->nb_samples, m_audio_codec_ctx->sample_rate, src_frame->sample_rate, AV_ROUND_UP);

	if (dst_nb_samples > m_max_dst_nb_samples) {
		av_freep(&m_audio_pcm_buffer[0]);
		int ret = av_samples_alloc(m_audio_pcm_buffer, nullptr, m_audio_codec_ctx->channels, dst_nb_samples, m_audio_codec_ctx->sample_fmt, 1);
		if (ret < 0)
			return false;
		m_max_dst_nb_samples = dst_nb_samples;
	}

	int nb_resample = swr_convert(m_audio_convert_ctx, m_audio_pcm_buffer, dst_nb_samples, (const uint8_t**)&src_frame->data, src_frame->nb_samples);
	if (nb_resample < 0)
		return false;

	av_audio_fifo_realloc(m_audio_fifo, av_audio_fifo_size(m_audio_fifo) + nb_resample);
	av_audio_fifo_write(m_audio_fifo, (void **)m_audio_pcm_buffer, nb_resample);

	return true;
}
bool FFAudioEncoder::OutputAudioData(AVPacket & pkt){

	MediaDataBitStream bs;
	bs.media_type = MediaType::AUDIO;
	if(m_audio_codec_ctx->codec_id == AV_CODEC_ID_AAC){
		if(!m_audio_output_buffer)
			m_audio_output_buffer = new unsigned char[1024*1024];
		int adts_len = MediaUtils::CreateAdts(m_audio_output_buffer, pkt.size, m_audio_codec_ctx->extradata, m_audio_codec_ctx->extradata_size);
		memcpy(m_audio_output_buffer + adts_len,pkt.data,pkt.size);
		bs.buffer = m_audio_output_buffer;
		bs.buffer_len = adts_len + pkt.size;
	}else{
		bs.buffer = pkt.data;
		bs.buffer_len = pkt.size;
	}
	bs.dts = pkt.dts < 0 ? 0 : pkt.dts;
	bs.pts = pkt.pts < 0 ? 0 : pkt.pts;
	bs.is_key = bs.pts == 0 ? true : false;
	if (m_cb) {
		m_cb(&bs, m_user_data);
	}

	return true;
}

