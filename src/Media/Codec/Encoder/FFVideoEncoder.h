
#ifndef FFVIDEOENCODER_H_
#define FFVIDEOENCODER_H_

#include "../../Media.h"

class FFVideoEncoder : public Encoder{
public:
	FFVideoEncoder();
	virtual ~FFVideoEncoder();
	virtual bool Start(MediaParams & params);
	virtual bool InputData(MediaData * data);
	virtual bool Stop();
private:
	struct AVCodecContext* m_video_codec_ctx = nullptr;
	struct AVFrame * m_video_frame = nullptr;
	struct AVFrame * m_tansformed_video_frame = nullptr;
	struct SwsContext * m_img_conver_ctx = nullptr;
private:
	bool EncodeVideo(MediaDataVideoBaseBand * baseband);
	struct AVFrame * TransformVideoFrame(struct AVFrame * src_frame);
	bool OutputVideoData(struct AVPacket & pkt);
	bool FLushEncoder();
};

#endif 
