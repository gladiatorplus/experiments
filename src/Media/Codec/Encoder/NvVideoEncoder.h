
#ifndef NVVIDEOENCODER_H_
#define NVVIDEOENCODER_H_

#include "../NVIDIA/NvEncodeAPI.h"
#include "dynlink_nvcuvid.h"
#include "../../Media.h"
#include <queue>

class NvVideoEncoder : public Encoder{
public:
	NvVideoEncoder();
	virtual ~NvVideoEncoder();
	virtual bool Start(MediaParams & params);
	virtual bool InputData(MediaData * data);
	virtual bool Stop();
private:
	void OutputFrame(NV_ENC_LOCK_BITSTREAM lockBitstreamData);
private:
	NVEncoderAPI                                        *m_nvencoder_api = nullptr;
	uint32_t                                             m_encoder_buffer_count = 0;
	void*                                                m_cuda_device = nullptr;
	EncodeBuffer                                         m_encoder_buffer[MAX_ENCODE_QUEUE];
	CNvQueue<EncodeBuffer>                               m_encoder_buffer_queue;
	EncodeConfig										 m_encode_config;
	std::queue<int64_t> 								 m_ptsqueue;
	CUvideoctxlock 										 m_ctx_lock = nullptr;
	bool												 m_inited = false;
private:
	NVENCSTATUS                                          Deinitialize();
	NVENCSTATUS                                          EncodeFrame(EncodeFrameConfig * frame);
	NVENCSTATUS                                          InitCuda(uint32_t device_id=0);
	NVENCSTATUS                                          AllocateIOBuffers(uint32_t width, uint32_t height, NV_ENC_BUFFER_FORMAT bufefr_fmt);
	NVENCSTATUS                                          ReleaseIOBuffers();
	NVENCSTATUS                                          FlushEncoder();
};

// NVEncodeAPI entry point
typedef NVENCSTATUS (NVENCAPI *MYPROC)(NV_ENCODE_API_FUNCTION_LIST*);

#endif 
