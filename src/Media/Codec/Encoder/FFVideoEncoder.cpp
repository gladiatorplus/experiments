
#include <sys/sysinfo.h>
#include "FFVideoEncoder.h"

extern "C"{
	#include "libavutil/opt.h"
	#include "libavcodec/avcodec.h"
	#include "libavformat/avformat.h"
	#include "libswscale/swscale.h"
	#include "libswresample/swresample.h"
	#include "libavutil/audio_fifo.h"
}

FFVideoEncoder::FFVideoEncoder() {
}

FFVideoEncoder::~FFVideoEncoder() {
	Stop();
}

bool FFVideoEncoder::Start(MediaParams & params){
	if(params.video.codec > VideoCodec::NONE){
		struct AVCodec  * avcodec = nullptr;
		AVCodecID codec_id = AV_CODEC_ID_NONE;
		switch (params.video.codec) {
			case VideoCodec::H264: {
				codec_id = AV_CODEC_ID_H264;
				break;
			}
			case VideoCodec::HEVC: {
				codec_id = AV_CODEC_ID_HEVC;
				break;
			}
			default:
				return false;
		}
		avcodec = avcodec_find_encoder(codec_id);

		if (avcodec == nullptr) {
			return false;
		}

		m_video_codec_ctx = avcodec_alloc_context3(avcodec);
		if (m_video_codec_ctx == nullptr) {
			return false;
		}

		if (codec_id == AV_CODEC_ID_H264) {
			const char * x264_preset_names[] = {
				"ultrafast", "superfast", "veryfast",
				"faster", "fast", "medium", "slow",
				"slower", "veryslow", "placebo"
			};
			av_opt_set(m_video_codec_ctx->priv_data, "preset", x264_preset_names[1], 0);
			av_opt_set(m_video_codec_ctx->priv_data, "tune", "zerolatency", 0);
			/*
			m_video_codec_ctx->profile = FF_PROFILE_H264_BASELINE;
			m_video_codec_ctx->rc_min_rate = params.video.bit_rate;
			m_video_codec_ctx->rc_max_rate = params.video.bit_rate;
			m_video_codec_ctx->rc_buffer_size = params.video.bit_rate / 5;
			*/
		}

		m_video_codec_ctx->bit_rate = params.video.bit_rate;

		m_video_codec_ctx->width = params.video.width;
		m_video_codec_ctx->height = params.video.height;

		m_video_codec_ctx->time_base.den = params.video.frame_rate_num;
		m_video_codec_ctx->time_base.num = params.video.frame_rate_den;

		m_video_codec_ctx->gop_size = params.video.gop_size;
		m_video_codec_ctx->max_b_frames = params.video.b_frames;

		m_video_codec_ctx->pix_fmt = AV_PIX_FMT_YUV420P;

		m_video_codec_ctx->codec_id = codec_id;
		m_video_codec_ctx->codec_type = AVMEDIA_TYPE_VIDEO;
		m_video_codec_ctx->sample_aspect_ratio = { 0,1 };
		int cpus = get_nprocs();
		m_video_codec_ctx->thread_count = cpus == 0 ? 4 : cpus;

		if (avcodec_open2(m_video_codec_ctx, avcodec, nullptr) < 0) {
			avcodec_free_context(&m_video_codec_ctx);
			return false;
		}
	}else
		return false;
	return true;
}
bool FFVideoEncoder::InputData(MediaData * data){
	if(data->media_data_type == MediaDataType::BASEBAND && data->media_type == MediaType::VIDEO){
		MediaDataVideoBaseBand * baseband = static_cast<MediaDataVideoBaseBand *>(data);
		return EncodeVideo(baseband);
	}else
		return false;
	return true;
}
bool FFVideoEncoder::Stop(){
	FLushEncoder();
	if (m_video_codec_ctx) {
		avcodec_close(m_video_codec_ctx);
		avcodec_free_context(&m_video_codec_ctx);
		m_video_codec_ctx = nullptr;
	}
	if (m_video_frame) {
		av_frame_free(&m_video_frame);
		m_video_frame = nullptr;
	}
	if (m_tansformed_video_frame) {
		avpicture_free((AVPicture*)m_tansformed_video_frame);
		av_frame_free(&m_tansformed_video_frame);
		m_tansformed_video_frame = nullptr;
	}
	if (m_img_conver_ctx) {
		sws_freeContext(m_img_conver_ctx);
		m_img_conver_ctx = nullptr;
	}
	return true;
}

bool FFVideoEncoder::EncodeVideo(MediaDataVideoBaseBand * baseband){
	if (!m_video_codec_ctx || !baseband)
		return false;

	AVPacket pkt;
	pkt.data = nullptr;
	pkt.size = 0;
	av_init_packet(&pkt);
	int got_picture = 0;
	AVFrame * send2encoder_frame = nullptr;
	if (!m_video_frame) {
		m_video_frame = av_frame_alloc();
	}

	m_video_frame->pts = baseband->pts;
	m_video_frame->width = baseband->width;
	m_video_frame->height = baseband->height;
	switch (baseband->fmt) {
		case VideoBaseBandFmt::YUV420P: {
			m_video_frame->format = AV_PIX_FMT_YUV420P;
			if(!baseband->buffer[0] || !baseband->buffer[1] || !baseband->buffer[2])
				return false;
			avpicture_fill((AVPicture*)m_video_frame, nullptr, (AVPixelFormat)m_video_frame->format, m_video_frame->width, m_video_frame->height);
			m_video_frame->data[0] = baseband->buffer[0];
			m_video_frame->data[1] = baseband->buffer[1];
			m_video_frame->data[2] = baseband->buffer[2];
			break;
		}
		default:
			return false;
	}

	if (m_video_frame->width != m_video_codec_ctx->width ||
		m_video_frame->height != m_video_codec_ctx->height ||
		m_video_frame->format != m_video_codec_ctx->pix_fmt) {
		send2encoder_frame = TransformVideoFrame(m_video_frame);
	}
	else
		send2encoder_frame = m_video_frame;

	if (avcodec_encode_video2(m_video_codec_ctx, &pkt, send2encoder_frame, &got_picture) < 0) {
		av_free_packet(&pkt);
		return false;
	}


	if (got_picture) {
		OutputVideoData(pkt);
		av_free_packet(&pkt);
	}

	return true;
}
AVFrame * FFVideoEncoder::TransformVideoFrame(AVFrame * src_frame){
	if (!m_video_codec_ctx)
		return nullptr;

	if (!m_img_conver_ctx) {
		m_img_conver_ctx = sws_getContext(src_frame->width, src_frame->height, (AVPixelFormat)src_frame->format,
			m_video_codec_ctx->width, m_video_codec_ctx->height,
			m_video_codec_ctx->pix_fmt, SWS_BILINEAR, nullptr, nullptr, nullptr); // SWS_BILINEAR SWS_BICUBIC
		if (!m_img_conver_ctx) {
			return nullptr;
		}
	}

	if (!m_tansformed_video_frame) {
		m_tansformed_video_frame = av_frame_alloc();
		if (!m_tansformed_video_frame) {
			return nullptr;
		}
		avpicture_alloc((AVPicture*)m_tansformed_video_frame, m_video_codec_ctx->pix_fmt, m_video_codec_ctx->width, m_video_codec_ctx->height);
	}
	int ret = sws_scale(m_img_conver_ctx, (const uint8_t* const *)src_frame->data,
		src_frame->linesize, 0, src_frame->height, m_tansformed_video_frame->data,
		m_tansformed_video_frame->linesize);

	if (ret < 0)
		return nullptr;

	m_tansformed_video_frame->pts = src_frame->pts;
	m_tansformed_video_frame->width = m_video_codec_ctx->width;
	m_tansformed_video_frame->height = m_video_codec_ctx->height;
	m_tansformed_video_frame->format = m_video_codec_ctx->pix_fmt;

	return m_tansformed_video_frame;
}
bool FFVideoEncoder::OutputVideoData(AVPacket & pkt){
	MediaDataBitStream bs;
	bs.media_type = MediaType::VIDEO;
	bs.pts = pkt.pts < 0 ? 0 : pkt.pts;
	bs.dts = pkt.dts < 0 ? 0 : pkt.dts;
	bs.buffer = pkt.data;
	bs.buffer_len = pkt.size;
	bs.is_key = pkt.flags & AV_PKT_FLAG_KEY ? true:false;
	if (m_cb) {
		m_cb(&bs, m_user_data);
	}
	av_free_packet(&pkt);
	return true;
}
bool FFVideoEncoder::FLushEncoder(){
	if(m_video_codec_ctx){
		int got_picture = 0;
		do {
			AVPacket pkt;
			pkt.data = nullptr;
			pkt.size = 0;
			av_init_packet(&pkt);
			if (avcodec_encode_video2(m_video_codec_ctx, &pkt, nullptr, &got_picture) < 0) {
				av_free_packet(&pkt);
				return false;
			}
			if (got_picture) {
				OutputVideoData(pkt);
				av_free_packet(&pkt);
			}
		} while (got_picture);
	}
	return true;
}
