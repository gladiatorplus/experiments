#ifndef FFAUDIOENCODER_H_
#define FFAUDIOENCODER_H_

#include "../../Media.h"

class FFAudioEncoder : public Encoder{
public:
	FFAudioEncoder();
	virtual ~FFAudioEncoder();
	virtual bool Start(MediaParams & params);
	virtual bool InputData(MediaData * data);
	virtual bool Stop();
private:
	struct AVCodecContext* m_audio_codec_ctx = nullptr;
	struct AVFrame * m_audio_frame = nullptr;
	struct AVFrame * m_resample_audio_frame = nullptr;
	struct SwrContext * m_audio_convert_ctx = nullptr;
	struct AVAudioFifo * m_audio_fifo = nullptr;
	unsigned char ** m_audio_pcm_buffer = nullptr;
	int	m_max_dst_nb_samples = 0;
	unsigned char * m_audio_output_buffer = nullptr;
private:
	bool FLushEncoder();
	bool EncodeAudio(MediaDataAudioBaseBand * baseband);
	bool ResamleAudio(struct AVFrame * src_frame);
	bool OutputAudioData(struct AVPacket & pkt);
};

#endif 
