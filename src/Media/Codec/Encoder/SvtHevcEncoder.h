#ifndef SVTHEVCENCODER_H
#define SVTHEVCENCODER_H

#include "../../Media.h"

#include <queue>
#include "EbApi.h"

class SvtHevcEncoder : public Encoder {
public:
	SvtHevcEncoder();
	virtual ~SvtHevcEncoder();
	virtual bool Start(MediaParams & params);
	virtual bool InputData(MediaData * data);
	virtual bool Stop();
private:
	int Encode(MediaData * data);
private:
	EB_COMPONENTTYPE * m_encoder = nullptr;
	std::queue<int64_t> m_dtsqueue;
	int m_eos_sent = 0;
};
#endif