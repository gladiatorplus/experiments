#include "SvtHevcEncoder.h"  
	
SvtHevcEncoder::SvtHevcEncoder(){
}
	
SvtHevcEncoder::~SvtHevcEncoder(){	
    Stop();
}


bool SvtHevcEncoder::Start(MediaParams & params){
    if(!m_encoder){
        EB_H265_ENC_CONFIGURATION config;
        EB_ERRORTYPE err = EbInitHandle(&m_encoder,this,&config);
        if(err != EB_ErrorNone){
            return false;
        }
        config.frameRateNumerator = params.video.frame_rate_num;
        config.frameRateDenominator = params.video.frame_rate_den;
        config.rateControlMode = 1;
        config.targetBitRate = params.video.bit_rate;
        config.sourceWidth = params.video.width;
        config.sourceHeight = params.video.height;
        config.encoderBitDepth = 8;
        config.encoderColorFormat = EB_YUV420;
        config.profile = 1;
        config.intraPeriodLength = params.video.gop_size;
        config.predStructure = 0;
        err = EbH265EncSetParameter(m_encoder,&config);
        if(err != EB_ErrorNone){
            EbDeinitHandle(m_encoder);
            m_encoder = nullptr;
            return false;
        }
        err = EbInitEncoder(m_encoder);
        if(err != EB_ErrorNone){
            EbDeinitHandle(m_encoder);
            m_encoder = nullptr;
            return false;
        }
        m_eos_sent = 0;
    }
    return true;
}

int SvtHevcEncoder::Encode(MediaData * data){
    EB_ERRORTYPE err = EB_ErrorNone;
    EB_BUFFERHEADERTYPE input_buffer;
    memset(&input_buffer,0,sizeof(EB_BUFFERHEADERTYPE));
    if(data){
        MediaDataVideoBaseBand * baseband = static_cast<MediaDataVideoBaseBand *>(data);
        EB_H265_ENC_INPUT pic;
        memset(&pic,0,sizeof(EB_H265_ENC_INPUT));
        pic.luma = baseband->buffer[0];
        pic.cb = baseband->buffer[1];
        pic.cr = baseband->buffer[2];
        pic.yStride = baseband->line_size[0];
        pic.cbStride = baseband->line_size[1];
        pic.crStride = baseband->line_size[2];
        input_buffer.pBuffer = (uint8_t*)&pic;
        input_buffer.pts = baseband->pts;
        m_dtsqueue.push(baseband->pts);

        err = EbH265EncSendPicture(m_encoder,&input_buffer);
        if(err != EB_ErrorNone){
            return -1;
        }
    }else{
        if(!m_eos_sent){
            input_buffer.nFlags = EB_BUFFERFLAG_EOS;
            err = EbH265EncSendPicture(m_encoder,&input_buffer);
            if(err != EB_ErrorNone){
                return -1;
            }
            m_eos_sent = 1;
        }
    }

    EB_BUFFERHEADERTYPE * output_buffer = nullptr;
    err = EbH265GetPacket(m_encoder,&output_buffer,m_eos_sent);
    if(err != EB_ErrorNone){
        return -1;
    }

    MediaDataBitStream bs;
    bs.media_type = MediaType::VIDEO;
    bs.pts = output_buffer->pts;
    bs.dts = m_dtsqueue.front();
    m_dtsqueue.pop();
    bs.buffer = output_buffer->pBuffer;
    bs.buffer_len = output_buffer->nFilledLen;
    bs.is_key = (output_buffer->sliceType == EB_IDR_PICTURE) ||
                    (output_buffer->sliceType == EB_I_PICTURE) ? true : false;
    if (m_cb) {
        m_cb(&bs, m_user_data);
    }

    EbH265ReleaseOutBuffer(&output_buffer);

    if(EB_BUFFERFLAG_EOS == output_buffer->nFlags){
        return 1;
    }

    return 0;
}

bool SvtHevcEncoder::InputData(MediaData * data){
    return Encode(data) < 0 ? false : true;
}
bool SvtHevcEncoder::Stop(){
    if(m_encoder){  
        int ret = 0;
        do{
            ret = Encode(nullptr);
            if(ret == 1){
                break;
            }
        }while(ret >= 0);
        
        EbDeinitEncoder(m_encoder);
        EbDeinitHandle(m_encoder);
        m_encoder = nullptr;
    }

    while(!m_dtsqueue.empty()){
		m_dtsqueue.pop();
	}
    
    return true;
}