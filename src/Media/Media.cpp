#include <stdio.h>

#include "Media.h"

#include "Codec/Decoder/FFAudioDecoder.h"
#include "Codec/Decoder/FFVideoDecoder.h"
#include "Codec/Decoder/NvVideoDecoder.h"

#include "Codec/Encoder/FFAudioEncoder.h"
#include "Codec/Encoder/FFVideoEncoder.h"
#include "Codec/Encoder/NvVideoEncoder.h"
#include "Codec/Encoder/SvtHevcEncoder.h"

#include "Filter/FrameRateFilter.h"
#include "Filter/RatioFilter.h"
#include "Filter/WatermarkFilter.h"
#include "Filter/SceneFilter.h"
#include "Filter/RotateFilter.h"
#include "Filter/CropFilter.h"

#include "Protocol/Input/FFInputProtocol.h"
#include "Protocol/Input/RTMPInputProtocol.h"

#include "Protocol/Output/FFOutputProtocol.h"
#include "Protocol/Output/RTMPOutputProtocol.h"


extern "C"{
	#include "libavcodec/avcodec.h"
	#include "libavformat/avformat.h"
	#include "libavfilter/avfilter.h"
	#include "libswscale/swscale.h"
}

static void FFLogCB(void*avcl, int level, const char*fmt, va_list vl) {
	char message[1024] = { 0 };
	vsnprintf(message, sizeof(message), fmt, vl);
	//printf(message);
	if(message[strlen(message)-1] == '\n')
		message[strlen(message)-1] = '\0';
	if(level == AV_LOG_ERROR) {
		SLOG_ERROR<<"FFLogCB,"<<message;
	}
}

bool DetectNVIDIA(){
	CUresult cu_result = CUDA_SUCCESS;
	cu_result = cuInit(0, __CUDA_API_VERSION, NULL);
	if(CUDA_SUCCESS != cu_result)
		return false;
	cu_result = cuvidInit();
	if(CUDA_SUCCESS != cu_result)
		return false;
	CUdevice device;
	int best_device = gpuGetMaxGflopsDeviceIdDRV();
	cu_result = cuDeviceGet(&device, best_device);
	if(CUDA_SUCCESS != cu_result)
		return false;
	return true;
}

void MediaGlobalInit(){
	av_register_all();
	avcodec_register_all();
	avfilter_register_all();
	avformat_network_init();
	av_log_set_callback(FFLogCB);
}



///////////////////////////////////////Encoder/////////////////////////////////////
Encoder * CreateEncoder(EncoderType type){
	switch (type) {
		case EncoderType::VIDEO_FFMPEG:
			return new FFVideoEncoder();
		case EncoderType::VIDEO_NVIDIA:
			return new NvVideoEncoder();
		case EncoderType::VIDEO_SVT_HEVC:
			return new SvtHevcEncoder();
		case EncoderType::AUDIO_FFMPEG:
			return new FFAudioEncoder();
		default:
			return nullptr;
	}
}
void Encoder::SetDataCB(MediaDataCB cb, void * user_data){
	m_cb = cb;
	m_user_data = user_data;
}

///////////////////////////////////////////Protocol/////////////////////////////////////////
//InputProtocol
InputProtocol * CreateInputProtocol(ProtocolType type){
	switch (type){
		case ProtocolType::RTMP:
			return new RTMPInputProtocol();
		case ProtocolType::OTHER:
			return new FFInputProtocol();
		default:
			return nullptr;
	}
}
void InputProtocol::SetDataCB(MediaDataCB cb,void * user_data){
	m_cb = cb;
	m_user_data = user_data;
}
IOStatus InputProtocol::GetCurrentStatus(){
	return m_current_status;
}
std::string InputProtocol::GetError(){
	return m_error;
}
bool InputProtocol::IdentifyLiveStream(std::string & source){
	if(source.find("rtmp://") != std::string::npos ||
		source.find("udp://") != std::string::npos ||
		source.find("rtsp://") != std::string::npos ||
		source.find("tcp://") != std::string::npos){
		return true;
	}else if(source.find("file://") != std::string::npos){
		return false;
	}else if(source.find("http://") != std::string::npos ||
			source.find("https://") != std::string::npos){
		AVFormatContext * fmt_ctx = avformat_alloc_context();
		avformat_open_input(&fmt_ctx, source.c_str(), 0, nullptr);
		if(!fmt_ctx)
			return false;
		if(avformat_find_stream_info(fmt_ctx, 0) < 0){
			avformat_close_input(&fmt_ctx);
			return false;
		}
		bool is_live = false;
		if(fmt_ctx->duration > 0){
			is_live = false;
		}else
			is_live = true;
		avformat_close_input(&fmt_ctx);
		return is_live;
	}else if(source.find(".m3u8") != std::string::npos){
		return false;
	}else
		return false;
}
//OutputProtocal
OutputProtocal * CreateOutputProtocal(ProtocolType type){
	switch (type){
		case ProtocolType::RTMP:
			return new RTMPOutputProtocol();
		case ProtocolType::OTHER:
			return new FFOutputProtocol();
		default:
			return nullptr;
	}
}
IOStatus OutputProtocal::GetStatus(){
	return m_status;
}

//////////////////////////////////////////////////Decoder////////////////////////////////////////
Decoder * CreateDecoder(DecoderType type){
	switch (type) {
		case DecoderType::VIDEO_FFMPEG:
			return new FFVideoDecoder();
		case DecoderType::VIDEO_NVIDIA:
			return new NvVideoDecoder();
		case DecoderType::AUDIO_FFMPEG:
			return new FFAudioDecoder();
		default:
			return nullptr;
	}
}
void Decoder::SetDataCB(MediaDataCB cb,void * user_data){
	m_cb = cb;
	m_user_data = user_data;
}

/////////////////////////////////////////////////Filter//////////////////////////////////////////
Filter * CreateFilter(FilterType type){
	switch (type) {
		case FilterType::WATERMARK:
			return new WatermarkFilter();
		case FilterType::RATIO:
			return new RatioFilter();
		case FilterType::FRAMERATE:
			return new FrameRateFilter();
		case FilterType::SCENE:
			return new SceneFilter();
		case FilterType::ROTATE:
			return new RotateFilter();
		case FilterType::CROP:
			return new CropFilter();
		default:
			return nullptr;
	}
}
void Filter::SetDataCB(MediaDataCB cb,void * user_data){
	m_cb = cb;
	m_user_data = user_data;
}

namespace MediaUtils{
int CreateAdts(unsigned char * data, int aac_body_size , unsigned char * aac_seq_header,int aac_seq_header_len) {
	if (aac_seq_header_len < 2 || !data || !aac_seq_header)
		return 0;
	int adts_len = 7;
	int aac_packet_size = aac_body_size + adts_len;
	int	obj_type, sr_index, ch_conf;
	unsigned char b0 = aac_seq_header[0];
	unsigned char b1 = aac_seq_header[1];
	obj_type = b0 >> 3;
	sr_index = ((b0 << 1) & 0x0f) | ((b1 & 0x80) >> 7);
	ch_conf = (b1 >> 3) & 0x0f;
	data[0] = 0xff;
	data[1] = 0xf1;
	data[2] = (((obj_type - 1) << 6) | (sr_index << 2) | ((ch_conf & 0x04) >> 2));
	data[3] = (((ch_conf & 0x03) << 6) | ((aac_packet_size >> 11) & 0x03));
	data[4] = (aac_packet_size >> 3);
	data[5] = ((aac_packet_size << 5) | 0x1f);
	data[6] = 0xfc;
	return adts_len;
}

//NAL Parser
#define SIZE_OF_NAL_UNIT_HDR    2
static bool HasStartCode (uint8_t *addr,uint8_t  zeros) {

	int i;
    for (i = 0; i < zeros; i++) {
        if (addr[i]) return false;
    }
    return addr[i] == 0x01 ? true : false;
}
void ScanNal(
    uint8_t     *start_addr,
    uint8_t     *nal_unit_header,
    int    		*nal_len,
    int    		*prefix_len,
	uint8_t		*buffer_end_ptr) {

    uint8_t offset = 0;
    uint8_t *p;

    p = start_addr;

    if (HasStartCode(p, 2)) {
        offset = 3;
    }
    else if (HasStartCode(p, 3)) {
        offset = 4;
    }

    *prefix_len = offset;
    p += offset;

    while (!HasStartCode(p, 2) && !HasStartCode(p, 3)){
    	if(p >= buffer_end_ptr)
    		break;
        p++;
    }

    nal_unit_header[0] = start_addr[offset];
    nal_unit_header[1] = start_addr[offset + 1];
    *nal_len  = (uint32_t) (p - start_addr);
}
//

int Annexb2Avcc(unsigned char * src,int src_len,unsigned char * dst){

	if(!src || src_len <= 0 || !dst)
		return false;

	int    nal_len;
	int    offset = 0;
	int    prefix_len = 0;
	unsigned char nal_unit_header[SIZE_OF_NAL_UNIT_HDR];
	int dst_index = 0;

	while (offset < src_len){
		ScanNal(src + offset, nal_unit_header, &nal_len, &prefix_len, src + src_len);
		int nal_size = nal_len-prefix_len;
		int be_nal_size = RESERVE_INT32((unsigned char*)&nal_size);
		memcpy(dst + dst_index, &be_nal_size,4);
		dst_index+=4;
		memcpy(dst + dst_index, src + offset + prefix_len,nal_len - prefix_len);
		dst_index += nal_len - prefix_len;
		offset = offset + nal_len;
	}
	return dst_index;

}

int Avcc2AnnexB(unsigned char * buffer,int buffer_len){
	if(!buffer)
		return 0;
	int index = 0;
	while(index < buffer_len){
		int nal_len = RESERVE_INT32(buffer + index);
		if(nal_len <= 0 || nal_len + 4 > buffer_len)
			return index;
		buffer[index++] = 0x00;
		buffer[index++] = 0x00;
		buffer[index++] = 0x00;
		buffer[index++] = 0x01;
		index += nal_len;
	}
	return index;
}
bool SavePicture(std::string & path,int width,int height,MediaDataVideoBaseBand & baseband){

	CreateFullPath(path);

	AVFrame *pic_frame = av_frame_alloc();
	if (!pic_frame) {
		SLOG_ERROR << "SavePicture, av_frame_alloc failed";
		return false;
	}

	AVPixelFormat src_fmt;
	switch (baseband.fmt) {
		case VideoBaseBandFmt::YUV420P:
			src_fmt = AV_PIX_FMT_YUV420P;
			break;
		default:
			return false;
	}

	avpicture_alloc((AVPicture*) pic_frame, AV_PIX_FMT_YUV420P, width, height);
	SwsContext * img_convert_ctx = sws_getContext(baseband.width, baseband.height,src_fmt,
			width, height, AV_PIX_FMT_YUV420P,
			SWS_FAST_BILINEAR, nullptr, nullptr, nullptr);

	if (!img_convert_ctx) {
		SLOG_ERROR << "SavePicture,sws_getContext failed";
		return false;
	}

	if(sws_scale(img_convert_ctx, (const uint8_t* const *)baseband.buffer,
			baseband.line_size, 0, baseband.height, pic_frame->data,
			pic_frame->linesize) < 0){
		SLOG_ERROR << "SavePicture,sws_scale error";
		return false;
	}

	pic_frame->width = width;
	pic_frame->height = height;
	pic_frame->format = AV_PIX_FMT_YUV420P;

	AVFormatContext*  format_ctx = avformat_alloc_context();
	format_ctx->oformat = av_guess_format("mjpeg", nullptr, nullptr);
	if (avio_open(&format_ctx->pb, path.c_str(), AVIO_FLAG_READ_WRITE) < 0) {
		SLOG_ERROR << "SavePicture::SavePicture,avio_open error";
		return false;
	}
	AVStream* avstream = avformat_new_stream(format_ctx, 0);
	if (!avstream) {
		return false;
	}
	AVCodecContext* codec_ctx = avstream->codec;

	codec_ctx->codec_id = format_ctx->oformat->video_codec;
	codec_ctx->codec_type = AVMEDIA_TYPE_VIDEO;
	codec_ctx->pix_fmt = AV_PIX_FMT_YUVJ420P;
	codec_ctx->width = width;
	codec_ctx->height = height;
	codec_ctx->time_base.num = 1;
	codec_ctx->time_base.den = 25;
	AVCodec*  codec = avcodec_find_encoder(codec_ctx->codec_id);
	if (!codec) {
		SLOG_ERROR << "SavePicture::Save,avcodec_find_encoder error";
		return false;
	}
	if (avcodec_open2(codec_ctx, codec, nullptr) < 0) {
		SLOG_ERROR << "SavePicture::Save,avcodec_open2 error";
		return false;
	}
	avformat_write_header(format_ctx, nullptr);
	int y_size = codec_ctx->width * codec_ctx->height;
	AVPacket pkt;
	av_new_packet(&pkt, y_size * 3);

	int got_picture = 0;
	if(avcodec_encode_video2(codec_ctx, &pkt, pic_frame, &got_picture) < 0){
		SLOG_ERROR << "SavePicture,avcodec_encode_video2 error";
		return false;
	}

	if (got_picture) {
		av_write_frame(format_ctx, &pkt);
	}
	av_free_packet(&pkt);
	av_write_trailer(format_ctx);

	if (avstream) {
		avcodec_close(avstream->codec);
	}
	avio_close(format_ctx->pb);
	avformat_free_context(format_ctx);

	if (pic_frame) {
		avpicture_free((AVPicture*) pic_frame);
		av_frame_free(&pic_frame);
	}
	sws_freeContext(img_convert_ctx);
	return true;
}
}
