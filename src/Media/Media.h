
#ifndef MEDIABASE_H_
#define MEDIABASE_H_

#include <iostream>
#include <stdint.h>
#include <vector>
#include <map>

#include "../Log.h"
#include "../Utils.h"
#include "../Config.h"

enum class VideoBaseBandFmt{
	NONE,
	YUV420P,
	NV12,
	BGR,
	BGRA
};

enum class AudioBaseBandFmt{
	NONE,
	S16
};

enum class VideoCodec {
	NONE,
	H264,
	HEVC,
	VP9
};

enum class AudioCodec {
	NONE,
	AAC,
	MP3,
	OPUS
};

enum class MuxerType {
	NONE,
	TS,
	MP4,
	FLV,
	MKV
};

struct MediaParams{
	MuxerType muxer_type = MuxerType::NONE;
	bool live_stream = false;
	struct VideoParams{
		VideoBaseBandFmt pix_fmt = VideoBaseBandFmt::NONE;
		VideoCodec codec = VideoCodec::NONE;
		int width = 0;
		int height = 0;
		int frame_rate_num = 0;
		int frame_rate_den = 0;
		int gop_size = 0;
		int b_frames = 0;
		int bit_rate = 0;
	}video;
	struct AudioParams{
		AudioCodec codec = AudioCodec::NONE;
		int bit_rate = 0;
		int channels = 0;
		int sample_rate = 0;
		int sample_size = 0;
		AudioBaseBandFmt sample_fmt = AudioBaseBandFmt::NONE;
	}audio;
};

struct MediaWatermark{
	std::string pic_path;
	std::string position = "0:0";
	std::string size = "80:80";
};

struct MediaFilterParams{
	MediaParams source_params;
	std::vector<MediaWatermark> watermarks;
	struct{
		bool blur = false;
		int width = 0;
		int height = 0;
	}ratio;
	struct{
		int frame_rate_num = 0;
		int frame_rate_den = 0;
	}framerate;
	struct{
		std::string url;
		int screen_width = 0;
		int screen_height = 0;
	}scence;
	struct {
		int angle = 0;
	}rotate;
	struct {
		int x = 0;
		int y = 0;
		int width = 0;
		int height = 0;
	}crop;
};

enum class MediaType{
	NONE,
	VIDEO,
	AUDIO
};

enum class MediaDataType{
	NONE,
	ES,
	BASEBAND,
	BASEBAND_HARDWARE
};

struct MediaData{
	MediaData(MediaDataType data_type,MediaType media_type = MediaType::NONE) : media_data_type(data_type),media_type(media_type){}
	virtual ~MediaData(){}
	MediaDataType media_data_type;
	MediaType media_type;
};

struct MediaDataBitStream : public MediaData{
	MediaDataBitStream():MediaData(MediaDataType::ES){}
	virtual ~MediaDataBitStream(){}
	unsigned char * buffer = nullptr;
	int buffer_len = 0;
	int64_t pts = 0;
	int64_t dts = 0;
	bool is_key = false;
};

struct MediaDataVideoBaseBand : public MediaData{
	MediaDataVideoBaseBand():MediaData(MediaDataType::BASEBAND,MediaType::VIDEO){}
	virtual ~MediaDataVideoBaseBand(){}
	int width = 0;
	int height = 0;
	int line_size[3] = {0};
	VideoBaseBandFmt fmt = VideoBaseBandFmt::NONE;
	int64_t pts = 0;
	unsigned char * buffer[3] = {0};
	void * hardware_ptr = nullptr;
	int bit_depth = 8;
};

struct MediaDataAudioBaseBand : public MediaData{
	MediaDataAudioBaseBand():MediaData(MediaDataType::BASEBAND,MediaType::AUDIO){}
	virtual ~MediaDataAudioBaseBand(){}
	int buffer_len = 0;
	AudioBaseBandFmt fmt = AudioBaseBandFmt::NONE;
	int channels = 0;
	int sample_rate = 0;
	int64_t pts = 0;
	unsigned char * buffer = nullptr;
};


typedef void(*MediaDataCB)(MediaData *data, void * user_data);

void MediaGlobalInit();
bool DetectNVIDIA();

//////////////////////////////////////////////////Encdoer////////////////////////////////
enum class EncoderType{
	NONE,
	//Video encoder
	VIDEO_FFMPEG,
	VIDEO_NVIDIA,
	VIDEO_SVT_HEVC,
	//Audio encoder
	AUDIO_FFMPEG
};
class Encoder {
public:
	Encoder() {}
	virtual ~Encoder() {}
	void SetDataCB(MediaDataCB cb, void * user_data);
	virtual bool Start(MediaParams & params) = 0;
	virtual bool InputData(MediaData * data) = 0;
	virtual bool Stop() = 0;
protected:
	MediaDataCB m_cb = nullptr;
	void * m_user_data = nullptr;
};
Encoder * CreateEncoder(EncoderType type);


///////////////////////////////////////////Protocol/////////////////////////////////////////////
enum class ProtocolType{
	NONE,
	RTMP,
	OTHER
};

enum class IOStatus{
	RUNNING,
	IDLE,
	ERROR
};

//InputProtocol
class InputProtocol{
public:
	InputProtocol() {};
	virtual ~InputProtocol() {};
	virtual bool Open(std::string & source, MediaParams & media_info) = 0;
	virtual bool Start() = 0;
	virtual bool Stop() = 0;
	void SetDataCB(MediaDataCB cb,void * user_data);
	IOStatus GetCurrentStatus();
	std::string GetError();
	bool IdentifyLiveStream(std::string & source);
protected:
	MediaDataCB m_cb = nullptr;
	void * m_user_data = nullptr;
	IOStatus m_current_status = IOStatus::IDLE;
	std::string m_error;
};
InputProtocol * CreateInputProtocol(ProtocolType type);
//OutputProtocal
class OutputProtocal {
public:
	OutputProtocal() {}
	virtual ~OutputProtocal() {}
	virtual bool Start(std::string & url,MediaParams & params) = 0;
	virtual bool InputData(MediaData * data) = 0;
	virtual bool Stop() = 0;
	IOStatus GetStatus();
protected:
	IOStatus m_status = IOStatus::IDLE;
};
OutputProtocal * CreateOutputProtocal(ProtocolType type);

//////////////////////////////////////////////////Decoder////////////////////////////////////////
enum class DecoderType{
	NONE,
	//Video decoder
	VIDEO_FFMPEG,
	VIDEO_NVIDIA,
	//Audio decoder
	AUDIO_FFMPEG
};
class Decoder{
public:
	Decoder(){}
	virtual ~Decoder(){}
	void SetDataCB(MediaDataCB cb,void * user_data);
	virtual bool Start(MediaParams & info) = 0;
	virtual int InputData(MediaData * data) = 0;
	virtual bool Stop() = 0;
protected:
	MediaDataCB m_cb = nullptr;
	void * m_user_data = nullptr;
};
Decoder * CreateDecoder(DecoderType type);

/////////////////////////////////////////////////Filter//////////////////////////////////////////
enum class FilterType{
	WATERMARK,
	RATIO,
	FRAMERATE,
	SCENE,
	ROTATE,
	CROP
};
class Filter{
public:
	Filter(){}
	virtual ~Filter(){}
	void SetDataCB(MediaDataCB cb,void * user_data);
	virtual bool Start(MediaFilterParams & params) = 0;
	virtual int InputData(MediaData * data) = 0;
	virtual bool Stop() = 0;
protected:
	MediaDataCB m_cb = nullptr;
	void * m_user_data = nullptr;
};
Filter * CreateFilter(FilterType type);

////////////////////////////////////////////////MediaUtils///////////////////////////////////////
namespace MediaUtils{
#define RESERVE_INT16(x) (unsigned int)(((*(x)) << 8) + (*(x + 1)))
#define RESERVE_INT24(x) (unsigned int)(((*(x)) << 16) + ((*(x + 1)) << 8) + (*(x + 2)))
#define RESERVE_INT32(x) (unsigned int)(((*(x)) << 24) + ((*(x + 1)) << 16) + ((*(x + 2)) << 8) + (*(x + 3)))
int CreateAdts(unsigned char * data, int aac_body_size , unsigned char * aac_seq_header,int aac_seq_header_len);
int Annexb2Avcc(unsigned char * src,int src_len,unsigned char * dst);
int Avcc2AnnexB(unsigned char * buffer,int buffer_len);
bool SavePicture(std::string & path,int width,int height,MediaDataVideoBaseBand & baseband);
void ScanNal(
    uint8_t     *start_addr,
    uint8_t     *nal_unit_header,
    int    		*nal_len,
    int    		*prefix_len,
	uint8_t		*buffer_end_ptr);
}
/////////////////////////////////////////////////////////////////////////////////////////////////

#endif
