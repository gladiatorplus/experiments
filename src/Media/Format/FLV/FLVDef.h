#ifndef FLVDEF_H
#define FLVDEF_H

enum class AMF0{
    NUMBER_MARKER,        
    BOOLEAN_MARKER,      
    STRING_MARKER,        
    OBJECT_MARKER,        
    MOVIECLIP_MARKER,     
    NULL_MARKER,          
    UNDEFINED_MARKER,    
    REFERENCE_MARKER,     
    ECMA_ARRAY_MARKER,    
    OBJECT_END_MARKER,   
    STRICT_ARRAY_MARKER,
    DATA_MARKER,         
    LONG_STRING_MARKER,   
    UNSUPPORTED_MARKER,  
    RECORDSET_MARKER,     
    XML_DOCUMENT_MARKER,  
    TYPED_OBJECT_MARKER,  
};

enum class FLVDataType{
	NONE,
	HEAD,
    TAG_AUDIO = 8,
	TAG_VIDEO = 9,
    TAG_SCRIPT = 18,
    TAG_AUDIO_SEQ,
    TAG_VIDEO_SEQ,
};

enum class FLVVideoCodec{
    NONE,
    MPEG2 = 1,
    H264 = 7,
    HEVC = 12
};

enum class FLVAudioCodec{
    NONE,
    MP3 = 2,
    MP2 = 6,
    AC3 = 9,
    AAC = 10
};

#pragma pack (1)
struct  HEVCDecoderConfRecHeader {
	uint8_t configurationVersion;
	union
	{
		struct{
			uint8_t general_profile_idc : 5;
			uint8_t general_tier_flag : 1;
			uint8_t general_profile_space : 2;
			uint32_t general_profile_compatibility_flags;
			uint32_t general_constraint_indicator_flags;
			uint16_t general_constraint_indicator_flags_32_47;
			uint8_t general_level_idc;
		}profile_tier_level;
		uint8_t profile_tier_level_buf[12];
	};
	uint8_t min_spatial_segmentation_idc_8_11 : 4;
	uint8_t reserved0 : 4;
	uint8_t min_spatial_segmentation_idc_0_7;
	uint8_t parallelismType : 2;
	uint8_t reserved1 : 6;
	uint8_t chromaFormat : 2;
	uint8_t reserved2 : 6;
	uint8_t bitDepthLumaMinus8 : 3;
	uint8_t reserved3 : 5;
	uint8_t bitDepthChromaMinus8 : 3;
	uint8_t reserved4 : 5;
	uint16_t avgFrameRate;
	uint8_t lengthSizeMinusOne : 2;
	uint8_t temporalIdNested : 1;
	uint8_t numTemporalLayers : 3;
	uint8_t constantFrameRate : 2;
	uint8_t numOfArrays;
};
#pragma pack ()

#define FLV_FILE_HEAD_SIZE 9
#define FLV_TAG_HEAD_SIZE 11
#define FLV_PRETAG_SIZE 4


#endif