#ifndef FLVMUXER_H_
#define FLVMUXER_H_

#include "../../Media.h"
#include "FLVDef.h"

typedef void(*FLVMuxerCB)(FLVDataType type,unsigned char * buffer,int buffer_len,void * user_data);

class FLVMuxer {
public:
	FLVMuxer();
	virtual ~FLVMuxer();
	bool Init(MediaParams & params);
	void SetDataCB(FLVMuxerCB cb,void * user_data);
	bool InputData(MediaData * data);
private:
	MediaParams m_params;
private:
	void GenerateHead();
	void GenerateTag(FLVDataType type,
					unsigned char * src,
					int src_len,int64_t pts,
					int64_t dts,
					bool is_key);
	int GenerateTagBody(FLVDataType type,
						unsigned char * dst,
						unsigned char * src,
						int src_len,
						int64_t cts,
						bool is_key);
	int GenerateTagHead(FLVDataType type,unsigned char * dst,int body_size,int64_t pts);
	int GenrateAVCDecodeConf(unsigned char * src,int src_len,unsigned char * dst);
	int GenrateAACDecodeConf(unsigned char * src,int src_len,unsigned char * dst);
	int GenrateHEVCDecodeConf(unsigned char * src,int src_len,unsigned char * dst);
private:
	FLVMuxerCB m_cb = nullptr;
	void * m_user_data = nullptr;
	unsigned char * m_flv_tag_buffer = nullptr;
	unsigned char * m_avcc_buffer = nullptr;
	bool m_video_seq_tag_send = false;
	bool m_audio_seq_tag_send = false;
};

#endif 
