#include "FLVDemuxer.h"  
	
#include <stddef.h>
#include <string.h>
#include <sys/stat.h>
#include <stdio.h>

#define ADD_SPACE(a) \
    do{ a[0] = 0;   \
        a[1] = 0;   \
        a[2] = 0;   \
        a[3] = 1;   \
        a += 4; }while(0)

FLVDemuxer::FLVDemuxer() {
}

FLVDemuxer::~FLVDemuxer() {
	if(m_es_buffer){
		delete [] m_es_buffer;
	}
}

static int64_t GetTagDataSize(unsigned char *tag_head){
    int size = 0;
    size += (unsigned char)tag_head[1];
    size <<= 8;
    size += (unsigned char)tag_head[2];
    size <<= 8;
    size += (unsigned char)tag_head[3];
    return size;
}

static int64_t GetTimeStamp(unsigned char *tag_head){
    int64_t  time_stamp = 0;
    time_stamp += (unsigned char)tag_head[7];
    time_stamp <<= 8;
    time_stamp += (unsigned char)tag_head[4];
    time_stamp <<= 8;
    time_stamp += (unsigned char)tag_head[5];
    time_stamp <<= 8;
    time_stamp += (unsigned char)tag_head[6];
    return time_stamp;
}

static int ParseAVCDecoderConfigRecord(uint8_t * buf, uint8_t *outbuf) {
	/*
    int configurationVersion = 1;
	int AVCProfileIndication = buf[1];
	int profile_compatibility = buf[2];
	int AVCLevelIndication = buf[3];
	int lenOfNALUnitLength = (buf[4] & 0x3) + 1;
    */
	int n_sps = buf[5] & 0x1f;
	uint8_t * p = &buf[6];
    int len = 0;
	for (int i = 0; i < n_sps; i++){
		int l = (p[0] << 8) + p[1];
        ADD_SPACE(outbuf);
        memcpy(outbuf, p+2, l);
        outbuf += l;
        len += l+4;
		p += 2 + l;
	}
	int n_pps = *p++;
	for (int i = 0; i < n_pps; i++){
		int l = (p[0] << 8) + p[1];
        ADD_SPACE(outbuf);
        memcpy(outbuf, p+2, l);
        outbuf += l;
        len += l + 4;
		p += 2 + l;
	}
    return len;
}

static int ParseHEVCHeader(unsigned char * data ,size_t len, unsigned char * buf){
	unsigned char * hevc_decoder_config_record_data = data + 5;
	int hevc_seq_len = 0;
	int current_read_index = 22;
	int num_of_arrays = hevc_decoder_config_record_data[current_read_index];
	current_read_index += 1;
	for(int i=0;i<num_of_arrays;i++){
		int NAL_unit_type = hevc_decoder_config_record_data[current_read_index] & 0x3f;
		current_read_index+=1;
		int num_nalus = RESERVE_INT16(hevc_decoder_config_record_data+current_read_index);
		for(int j=0;j<num_nalus;j++){
			current_read_index+=2;
			int nal_unit_len = RESERVE_INT16(hevc_decoder_config_record_data+current_read_index);
			current_read_index+=2;
			if(NAL_unit_type == (hevc_decoder_config_record_data[current_read_index]>>1) &&
					(NAL_unit_type == 0x20 || NAL_unit_type == 0x21 || NAL_unit_type == 0x22)){
				ADD_SPACE(buf);
				memcpy(buf,hevc_decoder_config_record_data+current_read_index,nal_unit_len);
				buf+=nal_unit_len;
				hevc_seq_len+=4+nal_unit_len;
			}
			current_read_index+=nal_unit_len;
		}
	}
	return hevc_seq_len;
}

void FLVDemuxer::Init(){
	m_cb = nullptr;
	m_userdata = nullptr;
	if(!m_es_buffer){
		m_es_buffer = new unsigned char[1024*1024*10];
	}
	m_audio_extra_data_len = 0;
}

FLVVideoCodec FLVDemuxer::GetVideoCodec(){
	return m_video_codec;
}

FLVAudioCodec FLVDemuxer::GetAudioCodec(){
	return m_audio_codec;
}

void FLVDemuxer::SetDataCB(FLVDemuxerCB cb,void * userdata){
	m_cb = cb;
	m_userdata = userdata;
}

int64_t FLVDemuxer::ParseTag(unsigned char * data, int64_t len){
	unsigned char *read = data;
	unsigned char  tag_type = 0;
	size_t tag_size = 0;
	int64_t tag_data_size=0;
	int64_t	coverd = 0;
	uint64_t time_stamp = 0;
	unsigned char video_code_id;
	unsigned char video_packet_type;
	unsigned char frame_type;
	unsigned char audio_code_id;
	unsigned char audio_packet_type;

	if (len < FLV_TAG_HEAD_SIZE) {
		return 0;
	}
	while ((len - coverd) > FLV_TAG_HEAD_SIZE) {
		read = data + coverd;
		tag_type = read[0];
		if(read[0] == 'F' && read[1] == 'L' && read[2] == 'V'){
			coverd += FLV_FILE_HEAD_SIZE + FLV_PRETAG_SIZE;
			continue;
		}
		if(tag_type != (unsigned char)FLVDataType::TAG_AUDIO && 
			tag_type != (unsigned char)FLVDataType::TAG_VIDEO && 
			tag_type != (unsigned char)FLVDataType::TAG_SCRIPT){
			coverd++;
			continue;
		}
		tag_data_size = GetTagDataSize(read);
		tag_size = FLV_TAG_HEAD_SIZE + tag_data_size + FLV_PRETAG_SIZE;
		if ((len - coverd) < (int64_t)tag_size)
			break;
		time_stamp = GetTimeStamp(read);
		if (tag_type == (unsigned char)FLVDataType::TAG_VIDEO) {
			frame_type = read[FLV_TAG_HEAD_SIZE] >> 4;
			video_code_id = read[FLV_TAG_HEAD_SIZE] & 0x0f;
			video_packet_type = read[FLV_TAG_HEAD_SIZE + 1];
			if(video_packet_type == 0){
				int es_len = 0;
				if(video_code_id == (unsigned char)FLVVideoCodec::H264){
					es_len = ParseAVCDecoderConfigRecord(read + FLV_TAG_HEAD_SIZE +5 , m_es_buffer);
					m_video_codec = FLVVideoCodec::H264;
				}else if(video_code_id == (unsigned char)FLVVideoCodec::HEVC){
					es_len = ParseHEVCHeader(read + FLV_TAG_HEAD_SIZE ,tag_data_size, m_es_buffer);
					m_video_codec = FLVVideoCodec::HEVC;
				}else if(video_code_id == (unsigned char)FLVVideoCodec::MPEG2){
					m_video_codec = FLVVideoCodec::MPEG2;
				}
				if(m_cb)
					m_cb(FLVDataType::TAG_VIDEO_SEQ,m_es_buffer,es_len,0,0,false,m_userdata);
			}else{
				int es_len = tag_data_size - 5;
				memcpy(m_es_buffer,read + FLV_TAG_HEAD_SIZE + 5,es_len);
				int32_t ts = RESERVE_INT24(read +13);
				int32_t cts = (ts + 0xff800000) ^ 0xff800000;
				if(cts>=10000){
					cts=0;
				}
				MediaUtils::Avcc2AnnexB(m_es_buffer,es_len);
				bool key = frame_type == 1 ? true : false;
				if(m_cb)
					m_cb(FLVDataType::TAG_VIDEO,m_es_buffer,es_len,time_stamp + cts,time_stamp,key,m_userdata);
			}
		}else if(tag_type == (unsigned char)FLVDataType::TAG_AUDIO){
			audio_code_id = read[FLV_TAG_HEAD_SIZE] >> 4;
			audio_packet_type = read[FLV_TAG_HEAD_SIZE + 1];
			if(audio_packet_type == 0){
				int es_len = 0;
				if(audio_code_id == (unsigned char)FLVAudioCodec::AAC){
					es_len = tag_data_size - 2;
					memcpy(m_es_buffer,read + FLV_TAG_HEAD_SIZE + 2,tag_data_size);
					m_audio_codec = FLVAudioCodec::AAC;
					memcpy(m_audio_extra_data,m_es_buffer,es_len);
					m_audio_extra_data_len = es_len;
				}else if(audio_code_id == (unsigned char)FLVAudioCodec::MP2){
					m_audio_codec = FLVAudioCodec::MP2;
				}else if(audio_code_id == (unsigned char)FLVAudioCodec::AC3){
					m_audio_codec = FLVAudioCodec::AC3;
				}else if(audio_code_id == (unsigned char)FLVAudioCodec::MP3){
					m_audio_codec = FLVAudioCodec::MP3;
				}	
				if(m_cb)
					m_cb(FLVDataType::TAG_AUDIO_SEQ,m_es_buffer,es_len,0,0,false,m_userdata);
			}else{
				int es_len = 0;
				if(audio_code_id == (unsigned char)FLVAudioCodec::AC3 || audio_code_id == (unsigned char)FLVAudioCodec::MP2){
					es_len = tag_data_size -1;
					memcpy(m_es_buffer,read + FLV_TAG_HEAD_SIZE + 1,es_len);
				}else if(audio_code_id == (unsigned char)FLVAudioCodec::AAC){
					es_len = tag_data_size - 2;
					int adts_len = MediaUtils::CreateAdts(m_es_buffer,es_len,m_audio_extra_data,m_audio_extra_data_len);
					memcpy(m_es_buffer + adts_len,read + FLV_TAG_HEAD_SIZE + 2,es_len);
					es_len += adts_len;
				}else{
					es_len = tag_data_size - 2;
					memcpy(m_es_buffer,read + FLV_TAG_HEAD_SIZE + 2,es_len);
				}
				if(m_cb)
					m_cb(FLVDataType::TAG_AUDIO,m_es_buffer,es_len,time_stamp,time_stamp,false,m_userdata);
			}
		}
		coverd += tag_size;
	}
	return coverd;
}
