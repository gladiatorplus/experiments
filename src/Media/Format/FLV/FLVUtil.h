#ifndef FLVUTIL_H_
#define FLVUTIL_H_

#include <string.h>
#include <sys/types.h>
#include <stdint.h>

#include "FLVDef.h"

u_char *FLVWrite8(u_char *buffer, unsigned int v);

u_char *FLVWrite16(u_char *buffer, unsigned int v);

u_char *FLVWrite24(u_char *buffer, unsigned int v);

u_char *FLVWrite32(u_char *buffer, uint32_t v);

u_char *FLVWrite64(u_char *buffer, uint64_t v);

u_char *FLVWriteAMFDouble(u_char *buffer, double rhs);

u_char *FLVWriteAMFBool(u_char *buffer, int rhs);

u_char *FLVWriteAMFStringNoMarker(u_char *buffer,const char *rhs);

u_char *FLVWriteAMFString(u_char *buffer,const char *rhs);

#endif 
