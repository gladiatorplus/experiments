
#include <string.h>
#include <stdio.h>
#include "FLVMuxer.h"
#include "FLVUtil.h"

FLVMuxer::FLVMuxer() {
}

FLVMuxer::~FLVMuxer() {
	if(m_flv_tag_buffer){
		delete [] m_flv_tag_buffer;
		m_flv_tag_buffer = nullptr;
	}
	if(m_avcc_buffer){
		delete [] m_avcc_buffer;
		m_avcc_buffer = nullptr;
	}
}

void FLVMuxer::GenerateHead(){
	unsigned char flv_head[13] = {0};
	flv_head[0] = 0x46;
	flv_head[1] = 0x4c;
	flv_head[2] = 0x56;
	flv_head[3] = 0x01;
	flv_head[4] = 0x00;

	if(m_params.audio.codec > AudioCodec::NONE)
		flv_head[4] |=0x04;
	if(m_params.video.codec > VideoCodec::NONE)
		flv_head[4] |=0x01;

	flv_head[5] = 0x00;
	flv_head[6] = 0x00;
	flv_head[7] = 0x00;
	flv_head[8] = 0x09;
	flv_head[9] = 0x00;
	flv_head[10] = 0x00;
	flv_head[11] = 0x00;
	flv_head[12] = 0x00;
	if(m_cb){
		m_cb(FLVDataType::HEAD,flv_head,13,m_user_data);
	}
}

int FLVMuxer::GenerateTagBody(FLVDataType type,
								unsigned char * dst,
								unsigned char * src,
								int src_len,
								int64_t cts,
								bool is_key){
	unsigned char * write_ptr = dst;
	if(type == FLVDataType::TAG_SCRIPT){
		write_ptr = FLVWriteAMFString(write_ptr,"onMetaData");
		write_ptr = FLVWrite8(write_ptr, (unsigned int)AMF0::ECMA_ARRAY_MARKER);

		write_ptr = FLVWrite32(write_ptr, 7);

		write_ptr = FLVWriteAMFStringNoMarker(write_ptr, "project");
		write_ptr = FLVWriteAMFString(write_ptr, "QSC");

		write_ptr = FLVWriteAMFStringNoMarker(write_ptr, "copyright");
		write_ptr = FLVWriteAMFString(write_ptr, "ChinamCloud");

		write_ptr = FLVWriteAMFStringNoMarker(write_ptr, "framerate");

		write_ptr = FLVWriteAMFDouble(write_ptr, m_params.video.frame_rate_den > 0 ?
				m_params.video.frame_rate_num / m_params.video.frame_rate_den : 0);

		write_ptr = FLVWriteAMFStringNoMarker(write_ptr, "width");
		write_ptr = FLVWriteAMFDouble(write_ptr, m_params.video.width);


		write_ptr = FLVWriteAMFStringNoMarker(write_ptr, "height");
		write_ptr = FLVWriteAMFDouble(write_ptr, m_params.video.height);

		write_ptr = FLVWriteAMFStringNoMarker(write_ptr, "audiochannels");
		write_ptr = FLVWriteAMFDouble(write_ptr, (double)m_params.audio.channels);

		write_ptr = FLVWriteAMFStringNoMarker(write_ptr, "audiosamplerate");
		write_ptr = FLVWriteAMFDouble(write_ptr, (double)m_params.audio.sample_rate);

		write_ptr = FLVWrite24(write_ptr, 0x000009);
	}else if(type == FLVDataType::TAG_VIDEO || type == FLVDataType::TAG_VIDEO_SEQ){
		unsigned char data8 = 0x00;
		if(is_key)
			data8 |= 0x10;
		else
			data8 |= 0x20;
		if(m_params.video.codec == VideoCodec::HEVC)
			data8 |= (unsigned char)FLVVideoCodec::HEVC;
		else if(m_params.video.codec == VideoCodec::H264)
			data8 |= (unsigned char)FLVVideoCodec::H264;
		else
			return 0;
		write_ptr = FLVWrite8(write_ptr, data8);
		data8 = 0x00;
		if(type != FLVDataType::TAG_VIDEO_SEQ)
			data8 = 0x01;
		write_ptr = FLVWrite8(write_ptr, data8);
		write_ptr = FLVWrite24(write_ptr, cts);
		memcpy(write_ptr,src,src_len);
		write_ptr += src_len;
	}else if(type == FLVDataType::TAG_AUDIO || type == FLVDataType::TAG_AUDIO_SEQ){
		unsigned char data8 = 0x00;
		if(m_params.audio.codec == AudioCodec::AAC){
			data8 |= (unsigned char)FLVAudioCodec::AAC << 4;
			data8 |= 0x0E;
		}else{
			if(m_params.audio.codec == AudioCodec::MP3){
				data8 |= (unsigned char)FLVAudioCodec::MP3 << 4;
			}else{
				return 0;
			}
			switch(m_params.audio.sample_rate){
				case 48000:
				case 44100:
					data8 |= 0x0E;
					break;
				case 22000:
					data8 |= 0x0A;
					break;
				case 11000:
					data8 |= 0x06;
					break;
				case 5500:
					data8 |= 0x02;
					break;
				default:
					return 0;
			}
		}

		if(m_params.audio.channels >= 2)
			data8 |= 0x01;
		else
			data8 |= 0x00;

		write_ptr = FLVWrite8(write_ptr, data8);
		data8 = 0x00;
		if(type != FLVDataType::TAG_AUDIO_SEQ)
			data8 = 0x01;
		write_ptr = FLVWrite8(write_ptr, data8);
		memcpy(write_ptr,src,src_len);
		write_ptr += src_len;
	}
	return write_ptr - dst;
}

int FLVMuxer::GenerateTagHead(FLVDataType type,unsigned char * dst,int body_size,int64_t pts){
	if(!dst)
		return 0;
	unsigned char * write_ptr = dst;
	if(type == FLVDataType::TAG_SCRIPT){
		write_ptr = FLVWrite8(write_ptr,(unsigned int)FLVDataType::TAG_SCRIPT);
		write_ptr = FLVWrite24(write_ptr,body_size);
		write_ptr = FLVWrite24(write_ptr,0);
		write_ptr = FLVWrite8(write_ptr,0);
		write_ptr = FLVWrite24(write_ptr,0);
	}else if(type == FLVDataType::TAG_VIDEO || type == FLVDataType::TAG_VIDEO_SEQ){
		write_ptr = FLVWrite8(write_ptr,(unsigned int)FLVDataType::TAG_VIDEO);
		write_ptr = FLVWrite24(write_ptr,body_size);
		write_ptr = FLVWrite24(write_ptr,pts);
		write_ptr = FLVWrite8(write_ptr,0);
		write_ptr = FLVWrite24(write_ptr,0);
	}else if(type == FLVDataType::TAG_AUDIO || type == FLVDataType::TAG_AUDIO_SEQ){
		write_ptr = FLVWrite8(write_ptr,(unsigned int)FLVDataType::TAG_AUDIO);
		write_ptr = FLVWrite24(write_ptr,body_size);
		write_ptr = FLVWrite24(write_ptr,pts);
		write_ptr = FLVWrite8(write_ptr,0);
		write_ptr = FLVWrite24(write_ptr,0);
	}
	return write_ptr - dst;
}

void FLVMuxer::GenerateTag(FLVDataType type,unsigned char * src,int src_len,int64_t pts,int64_t dts,bool is_key){
	if(type == FLVDataType::TAG_SCRIPT){
		int body_size = GenerateTagBody(type,m_flv_tag_buffer + FLV_TAG_HEAD_SIZE,nullptr,0,0,false);
		int head_size = GenerateTagHead(type,m_flv_tag_buffer,body_size,0);
		int tag_len = head_size+body_size;
		unsigned char * write_ptr = m_flv_tag_buffer + tag_len;
		FLVWrite32(write_ptr, tag_len);
		if(m_cb){
			m_cb(type,m_flv_tag_buffer,tag_len+4,m_user_data);
		}
	}else if(type == FLVDataType::TAG_VIDEO || type == FLVDataType::TAG_VIDEO_SEQ){
		int body_size = GenerateTagBody(type,m_flv_tag_buffer + FLV_TAG_HEAD_SIZE,src,src_len,pts-dts,is_key);
		int head_size = GenerateTagHead(type,m_flv_tag_buffer,body_size,dts);
		int tag_len = head_size+body_size;
		unsigned char * write_ptr = m_flv_tag_buffer + tag_len;
		FLVWrite32(write_ptr, tag_len);
		if(m_cb){
			m_cb(type,m_flv_tag_buffer,tag_len+4,m_user_data);
		}
	}else if(type == FLVDataType::TAG_AUDIO || type == FLVDataType::TAG_AUDIO_SEQ){
		int body_size = GenerateTagBody(type,m_flv_tag_buffer + FLV_TAG_HEAD_SIZE,src,src_len,0,is_key);
		int head_size = GenerateTagHead(type,m_flv_tag_buffer,body_size,dts);
		int tag_len = head_size+body_size;
		unsigned char * write_ptr = m_flv_tag_buffer + tag_len;
		FLVWrite32(write_ptr, tag_len);
		if(m_cb){
			m_cb(type,m_flv_tag_buffer,tag_len+4,m_user_data);
		}
	}
}

int FLVMuxer::GenrateAACDecodeConf(unsigned char * src,int src_len,unsigned char * dst){
	if(!src || src_len < 7 || !dst)
		return false;
	int header = src[0];
	header |= src[1]>>4;
	int profile = ((src[2] & 0xff) >> 6);
	int sampling_frequency_index = ((src[2] & 0xff) >> 2) & 0x0f;
	int channel_configuration =((src[2] & 0x01) << 2) + (((src[3] & 0xff) >> 6) & 0x03);
	dst[0] = ((profile + 1) << 3)|((sampling_frequency_index & 0x0e) >> 1);
	dst[1] = ((sampling_frequency_index & 0x1) << 7)|(channel_configuration << 3);
	return 2;
}

int FLVMuxer::GenrateAVCDecodeConf(unsigned char * src,int src_len,unsigned char * dst){
	if(!src || !dst)
		return 0;
	unsigned char sps[100] = {0};
	unsigned char pps[100] = {0};
	int    nal_len;
	int    offset = 0;
	int    prefix_len = 0;
	unsigned char nal_unit_header[2] = {0};

	int sps_len = 0,pps_len = 0;

	while (offset < src_len){
		MediaUtils::ScanNal(src+offset, nal_unit_header, &nal_len, &prefix_len, src + src_len);
		unsigned char nal_type = nal_unit_header[0] & 0x1F;
		if(nal_type == 0x07){
			memcpy(sps,src+offset+prefix_len,nal_len-prefix_len);
			sps_len = nal_len-prefix_len;
		}
		else if(nal_type == 0x08){
			memcpy(pps,src+offset+prefix_len,nal_len-prefix_len);
			pps_len = nal_len-prefix_len;
		}
		offset += nal_len;
	}

	if(sps_len == 0 || pps_len == 0)
		return 0;

	unsigned char * write_ptr = dst;
	write_ptr = FLVWrite8(write_ptr, 1);
	write_ptr = FLVWrite8(write_ptr, sps[1]);
	write_ptr = FLVWrite8(write_ptr, sps[2]);
	write_ptr = FLVWrite8(write_ptr, sps[3]);

	write_ptr[0] = 0xFF;
	write_ptr[1] = 0xE1;
	write_ptr += 2;
	write_ptr = FLVWrite16(write_ptr, sps_len);
	memcpy(write_ptr,sps,sps_len);
	write_ptr += sps_len;

	write_ptr = FLVWrite8(write_ptr, 1);
	write_ptr = FLVWrite16(write_ptr, pps_len);
	memcpy(write_ptr, pps,pps_len);
	write_ptr += pps_len;

	return write_ptr - dst;
}

int FLVMuxer::GenrateHEVCDecodeConf(unsigned char * src,int src_len,unsigned char * dst){
	if(!src || !dst)
		return 0;
	unsigned char sps[100] = {0};
	unsigned char pps[100] = {0};
	unsigned char vps[100] = {0};
	int    nal_len;
	int    offset = 0;
	int    prefix_len = 0;
	unsigned char nal_unit_header[2] = {0};

	int sps_len = 0,pps_len = 0,vps_len = 0;

	while (offset < src_len){
		MediaUtils::ScanNal(src+offset, nal_unit_header, &nal_len, &prefix_len, src + src_len);
		unsigned char nal_unit_type = (nal_unit_header[0] & 0x7E) >> 1;
		if(nal_unit_type == 0x21){
			memcpy(sps,src+offset+prefix_len,nal_len-prefix_len);
			sps_len = nal_len-prefix_len;
		}
		else if(nal_unit_type == 0x22){
			memcpy(pps,src+offset+prefix_len,nal_len-prefix_len);
			pps_len = nal_len-prefix_len;
		}else if(nal_unit_type == 0x20){
			memcpy(vps,src+offset+prefix_len,nal_len-prefix_len);
			vps_len = nal_len-prefix_len;
		}
		offset += nal_len;
	}

	if(sps_len == 0 || pps_len == 0 || vps_len == 0)
		return 0;

	uint8_t * profile_tier_level = sps + 4 + 2 + 1;
	HEVCDecoderConfRecHeader * p_rec = (HEVCDecoderConfRecHeader*)dst;
	p_rec->configurationVersion = 1;
	int profile_tier_level_header_len = 12;
	memcpy(p_rec->profile_tier_level_buf, profile_tier_level, profile_tier_level_header_len);
	p_rec->reserved0 = 15;
	p_rec->min_spatial_segmentation_idc_8_11 = 0;
	p_rec->min_spatial_segmentation_idc_0_7 = 0;
	p_rec->reserved1 = 63;
	p_rec->parallelismType = 0;
	p_rec->reserved2 = 63;
	p_rec->chromaFormat = 1;
	p_rec->reserved3 = 31;
	p_rec->bitDepthLumaMinus8 = 0;
	p_rec->reserved4 = 31;
	p_rec->bitDepthChromaMinus8 = 0;
	p_rec->avgFrameRate = 0;
	p_rec->constantFrameRate = 0;
	p_rec->numTemporalLayers = 0;
	p_rec->temporalIdNested = 1;
	p_rec->lengthSizeMinusOne = 3;
	p_rec->numOfArrays = 3;
	uint8_t * array = ((uint8_t*)p_rec) + sizeof(HEVCDecoderConfRecHeader);

	*array++ = 0x20; //VPS
	*array++ = 0;
	*array++ = 1;
	int l = vps_len;
	*array++ = l>>8;
	*array++= l&0xff;
	memcpy(array, vps, l);
	array += l;

	*array++ = 0x21; //SPS
	*array++ = 0;
	*array++ = 1;
	l = sps_len;
	*array++ = l>>8;
	*array++= l&0xff;
	memcpy(array, sps, l);
	array += l;

	*array++ = 0x22; //PPS
	*array++ = 0;
	*array++ = 1;
	l = pps_len;
	*array++ = l>>8;
	*array++= l&0xff;
	memcpy(array, pps, l);
	array += l;

	return array - dst;
}

void FLVMuxer::SetDataCB(FLVMuxerCB cb,void * user_data){
	m_cb = cb;
	m_user_data = user_data;
}

bool FLVMuxer::Init(MediaParams & params){
	memcpy(&m_params,&params,sizeof(MediaParams));
	if(!m_flv_tag_buffer)
		m_flv_tag_buffer = new unsigned char[1024*1024*10];
	if(!m_avcc_buffer)
		m_avcc_buffer = new unsigned char[1024*1024*10];

	m_video_seq_tag_send = false;
	m_audio_seq_tag_send = false;

	GenerateHead();
	GenerateTag(FLVDataType::TAG_SCRIPT,nullptr,0,0,0,false);

	return true;
}

bool FLVMuxer::InputData(MediaData * data){
	MediaDataBitStream * bs = static_cast<MediaDataBitStream*>(data);
	if(bs->media_type == MediaType::VIDEO){
		unsigned char seq_data[1024] = {0};
		if(!m_video_seq_tag_send){
			int dec_conf_len = 0;
			if(m_params.video.codec == VideoCodec::H264){
				dec_conf_len = GenrateAVCDecodeConf(bs->buffer,bs->buffer_len,seq_data);
			}else if(m_params.video.codec == VideoCodec::HEVC){
				dec_conf_len = GenrateHEVCDecodeConf(bs->buffer,bs->buffer_len,seq_data);
			}else
				return false;
			if(dec_conf_len <= 0)
				return false;
			GenerateTag(FLVDataType::TAG_VIDEO_SEQ,seq_data,dec_conf_len,0,0,true);
			m_video_seq_tag_send = true;
		}

		if(!m_video_seq_tag_send)
			return false;

		int avcc_len = MediaUtils::Annexb2Avcc(bs->buffer,bs->buffer_len,m_avcc_buffer);
		GenerateTag(FLVDataType::TAG_VIDEO,m_avcc_buffer,avcc_len,bs->pts,bs->dts,bs->is_key);
	}
	else if(bs->media_type == MediaType::AUDIO){
		if(m_params.audio.codec == AudioCodec::AAC){
			if(!m_audio_seq_tag_send){
				unsigned char seq_data[1024] = {0};
				int dec_conf_len = 	GenrateAACDecodeConf(bs->buffer,bs->buffer_len,seq_data);
				if(dec_conf_len <= 0)
					return false;
				GenerateTag(FLVDataType::TAG_AUDIO_SEQ,seq_data,dec_conf_len,0,0,true);
				m_audio_seq_tag_send = true;
			}
			GenerateTag(FLVDataType::TAG_AUDIO,bs->buffer+7,bs->buffer_len-7,bs->pts,bs->dts,bs->is_key);
		}else
			GenerateTag(FLVDataType::TAG_AUDIO,bs->buffer,bs->buffer_len,bs->pts,bs->dts,bs->is_key);
	}
	else
		return false;

	return true;
}
