

#include "FLVUtil.h"

u_char * FLVWrite8(u_char *buffer, unsigned int v) {
    buffer[0] = (uint8_t)v;
    return buffer + 1;
}

u_char * FLVWrite16(u_char *buffer, unsigned int v) {
    buffer[0] = (uint8_t)(v >> 8);
    buffer[1] = (uint8_t)(v >> 0);

    return buffer + 2;
}

u_char * FLVWrite24(u_char *buffer, unsigned int v) {
    buffer[0] = (uint8_t)(v >> 16);
    buffer[1] = (uint8_t)(v >> 8);
    buffer[2] = (uint8_t)(v >> 0);

    return buffer + 3;
}

u_char * FLVWrite32(u_char *buffer, uint32_t v) {
    buffer[0] = (uint8_t)(v >> 24);
    buffer[1] = (uint8_t)(v >> 16);
    buffer[2] = (uint8_t)(v >> 8);
    buffer[3] = (uint8_t)(v >> 0);

    return buffer + 4;
}

u_char * FLVWrite64(u_char *buffer, uint64_t v) {
	FLVWrite32(buffer + 0, (uint32_t)(v >> 32));
	FLVWrite32(buffer + 4, (uint32_t)(v >> 0));

    return buffer + 8;
}

u_char * FLVWriteAMFDouble(u_char *buffer, double rhs) {
    union {
        uint64_t    val_integer;
        double      val_double;
    } val;

    buffer = FLVWrite8(buffer, (unsigned int)AMF0::NUMBER_MARKER);
    val.val_double = rhs;
    buffer = FLVWrite64(buffer, val.val_integer);

    return buffer;
}

u_char * FLVWriteAMFBool(u_char *buffer, int rhs) {
    buffer = FLVWrite8(buffer, (unsigned int)AMF0::BOOLEAN_MARKER);
    buffer = FLVWrite8(buffer, rhs == 0 ? 0 : 1);

    return buffer;
}

u_char * FLVWriteAMFStringNoMarker(u_char *buffer,  const char *rhs) {
    size_t size = strlen(rhs);
    buffer = FLVWrite16(buffer, size);
    memcpy(buffer, rhs, size);
    buffer += size;

    return buffer;
}

u_char * FLVWriteAMFString(u_char *buffer, const char *rhs) {
    size_t size = strlen(rhs);

    if(size == 0) {
        buffer = FLVWrite8(buffer, (unsigned int)AMF0::NULL_MARKER);

    } else if(size < 65536) {
        buffer = FLVWrite8(buffer, (unsigned int)AMF0::STRING_MARKER);
        buffer = FLVWrite16(buffer, size);

    } else {
        buffer = FLVWrite8(buffer, (unsigned int)AMF0::LONG_STRING_MARKER);
        buffer = FLVWrite32(buffer, size);
    }

    memcpy(buffer, rhs, size);
    buffer += size;

    return buffer;
}
