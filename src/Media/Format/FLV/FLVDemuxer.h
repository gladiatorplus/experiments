#ifndef FLVDEMUXER_H
#define FLVDEMUXER_H

#include <stdint.h>
#include <stdio.h>

#include "../../Media.h"

#include "FLVDef.h"

typedef void (*FLVDemuxerCB)(FLVDataType type,unsigned char * buffer,int bufferlen,int64_t pts,int64_t dts,bool key,void * userdata);

class FLVDemuxer {
public:
	FLVDemuxer();
	virtual ~FLVDemuxer();
	void Init();
	void SetDataCB(FLVDemuxerCB cb,void * userdata);
	int64_t ParseTag(unsigned char * data, int64_t len);
	FLVVideoCodec GetVideoCodec();
	FLVAudioCodec GetAudioCodec();
private:
	FLVDemuxerCB m_cb = nullptr;
	void * m_userdata = nullptr;
	unsigned char * m_es_buffer = nullptr;
private:
	FLVVideoCodec m_video_codec = FLVVideoCodec::NONE;
	FLVAudioCodec m_audio_codec = FLVAudioCodec::NONE;
	unsigned char m_audio_extra_data[1024] = {0};
	int m_audio_extra_data_len = 0;
};

#endif