#ifndef RTMPINPUTPROTOCOL_H
#define RTMPINPUTPROTOCOL_H

#include "../../Media.h"

#include "../../Format/FLV/FLVDemuxer.h"

extern "C"{
	#include "rtmp.h"
};

	
class RTMPInputProtocol : public InputProtocol{
public:
	RTMPInputProtocol();
	~RTMPInputProtocol();
	virtual bool Open(std::string & source, MediaParams & media_info);
	virtual bool Start();
	virtual bool Stop();
private:
	static void FLVDemuxerCBFunc(FLVDataType type,unsigned char * buffer,int bufferlen,int64_t pts,int64_t dts,bool key,void * userdata);
	static void * WorkThreadFunc(void * arg);
private:
	RTMP * ConnectRTMP(char * url);
	void CloseRTMP(RTMP * rtmp);
	bool Pull();
	void AllocMemory();
	void FreeMemory();
private:
	pthread_t m_worker_thread = 0;
	bool m_stop = false;
	bool m_probe = false;
	std::string m_source;
private:
	char * m_flv_buffer = nullptr;
	unsigned char * m_audio_buffer = nullptr;
	int m_audio_buffer_len = 0;
	unsigned char * m_video_buffer = nullptr;
	int m_video_buffer_len = 0;
};
#endif