#include "FFInputProtocol.h"

extern "C"{
	#include "libavcodec/avcodec.h"
	#include "libavformat/avformat.h"
	#include "libavutil/time.h"
}

FFInputProtocol::FFInputProtocol() {
}

FFInputProtocol::~FFInputProtocol() {
	Stop();
}

int FFInputProtocol::InterruptCB(void *arg) {
	FFInputProtocol * obj = (FFInputProtocol*)arg;
	time_t current_time = time(nullptr);
	if(current_time - obj->m_timeout_start_time > 5)
		return 1;
    return 0;
}

bool FFInputProtocol::Open(std::string & source, MediaParams & media_info){
	Stop();
	media_info.live_stream = IdentifyLiveStream(source);
	if(!m_fmt_ctx)
		m_fmt_ctx = avformat_alloc_context();
	m_fmt_ctx->interrupt_callback.callback = InterruptCB;
	m_fmt_ctx->interrupt_callback.opaque = this;
	m_timeout_start_time = time(nullptr);

	AVDictionary *options = nullptr;
	if(source.find(".m3u8") != std::string::npos){
		av_dict_set(&options, "live_start_index", "0", 0);
	}
	if(source.find("http://") != std::string::npos || source.find("https://") != std::string::npos){
		av_dict_set(&options, "reconnect", "1", 0);
		av_dict_set(&options, "reconnect_at_eof", "1", 0);
		av_dict_set(&options, "reconnect_streamed", "1", 0);
		av_dict_set(&options, "reconnect_delay_max", "2", 0);
	}

	avformat_open_input(&m_fmt_ctx, source.c_str(), 0, &options);
	if(options){
		av_dict_free(&options);
	}
	std::string format_name;
	if(!m_fmt_ctx){
		m_error = "avformat_open_input error";
		SLOG_ERROR<<"FFStreamDemuxer::Open,"<<m_error;
		goto FAIL;
	}

	m_fmt_ctx->probesize2 = 1024 * 1024;
	m_fmt_ctx->max_analyze_duration2 = 30 * AV_TIME_BASE;

	if(avformat_find_stream_info(m_fmt_ctx, 0) < 0){
		m_error = "avformat_find_stream_info error";
		SLOG_ERROR<<"FFStreamDemuxer::Open,"<<m_error;
		goto FAIL;
	}

	format_name = m_fmt_ctx->iformat->name;
	if(format_name.find("flv") != std::string::npos){
		media_info.muxer_type = MuxerType::FLV;
	}else if(format_name.find("mp4") != std::string::npos){
		media_info.muxer_type = MuxerType::MP4;
	}else if(format_name.find("mpegts") != std::string::npos){
		media_info.muxer_type = MuxerType::TS;
	}else if(format_name.find("hls") != std::string::npos){
		media_info.muxer_type = MuxerType::TS;
	}else if(format_name.find("matroska") != std::string::npos){
		media_info.muxer_type = MuxerType::MKV;
	}


	for (unsigned int i = 0; i < m_fmt_ctx->nb_streams; i++){
		if (m_fmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
			m_video_index = i;
			if(m_fmt_ctx->streams[i]->codec->codec_id == AV_CODEC_ID_H264){
				media_info.video.codec = VideoCodec::H264;
			}else if(m_fmt_ctx->streams[i]->codec->codec_id == AV_CODEC_ID_HEVC){
				media_info.video.codec = VideoCodec::HEVC;
			}else if(m_fmt_ctx->streams[i]->codec->codec_id == AV_CODEC_ID_VP9){
				media_info.video.codec = VideoCodec::VP9;
			}else{
				m_error = "unsupported video codec";
				SLOG_ERROR<<"FFStreamDemuxer::Open,"<<m_error;
				goto FAIL;
			}
			media_info.video.width = m_fmt_ctx->streams[i]->codec->width;
			media_info.video.height = m_fmt_ctx->streams[i]->codec->height;
			media_info.video.bit_rate = m_fmt_ctx->streams[i]->codec->bit_rate;
			media_info.video.gop_size = m_fmt_ctx->streams[i]->codec->gop_size;
			media_info.video.frame_rate_den = m_fmt_ctx->streams[i]->avg_frame_rate.den;
			media_info.video.frame_rate_num = m_fmt_ctx->streams[i]->avg_frame_rate.num;
			switch (m_fmt_ctx->streams[i]->codec->pix_fmt) {
				case AV_PIX_FMT_YUV420P:
					media_info.video.pix_fmt = VideoBaseBandFmt::YUV420P;
					break;
				default:
					media_info.video.pix_fmt = VideoBaseBandFmt::YUV420P;
					break;
			}
		}
		if (m_fmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
			m_audio_index = i;
			if(m_fmt_ctx->streams[i]->codec->codec_id == AV_CODEC_ID_AAC){
				media_info.audio.codec = AudioCodec::AAC;
			}else if(m_fmt_ctx->streams[i]->codec->codec_id == AV_CODEC_ID_MP3){
				media_info.audio.codec = AudioCodec::MP3;
			}else if(m_fmt_ctx->streams[i]->codec->codec_id == AV_CODEC_ID_OPUS){
				media_info.audio.codec = AudioCodec::OPUS;
			}else{
				m_error = "unsupported audio codec";
				SLOG_ERROR<<"FFStreamDemuxer::Open,"<<m_error;
				goto FAIL;
			}
			media_info.audio.bit_rate = m_fmt_ctx->streams[i]->codec->bit_rate;
			media_info.audio.channels = m_fmt_ctx->streams[i]->codec->channels;
			media_info.audio.sample_rate = m_fmt_ctx->streams[i]->codec->sample_rate;
			media_info.audio.sample_size = m_fmt_ctx->streams[i]->codec->frame_size;
		}
	}

	m_current_media_info = media_info;

	av_dump_format(m_fmt_ctx, 0, source.c_str(), 0);
	m_source_url = source;
	return true;
FAIL:
	m_current_status = IOStatus::ERROR;
	return false;
}



bool FFInputProtocol::StartPull(){
	if(!m_fmt_ctx)
		return false;
	m_current_status = IOStatus::RUNNING;
	AVCodecContext * video_codec_ctx = nullptr;
	AVCodecContext * audio_codec_ctx = nullptr;
	if(m_video_index >= 0)
		video_codec_ctx = m_fmt_ctx->streams[m_video_index]->codec;
	if(m_audio_index >= 0)
		audio_codec_ctx = m_fmt_ctx->streams[m_audio_index]->codec;
	AVBitStreamFilterContext* video_bsfc = nullptr;
	bool need_create_adts = false;

	switch(m_current_media_info.muxer_type){
		case MuxerType::MKV:
		case MuxerType::MP4:
		case MuxerType::FLV:{
			if(m_current_media_info.video.codec == VideoCodec::H264) {
				video_bsfc =  av_bitstream_filter_init("h264_mp4toannexb");
				if(!video_bsfc)
					return false;
			}
			if(m_current_media_info.video.codec == VideoCodec::HEVC){
				video_bsfc =  av_bitstream_filter_init("hevc_mp4toannexb");
				if(!video_bsfc)
					return false;
			}
			if(m_current_media_info.audio.codec == AudioCodec::AAC){
				need_create_adts = true;
			}
			break;
		}
		default:
			break;
	}

	bool nomal_exit = true;

	AVPacket pkt;
	int64_t start_time = av_gettime();
	int64_t first_dts_time = -1;

	while(!m_stop){
		m_timeout_start_time = time(nullptr);

		int ret = av_read_frame(m_fmt_ctx, &pkt);
		if(ret < 0){
			if(ret == AVERROR_EOF){
				m_error = "av_read_frame AVERROR_EOF";
				SLOG_ERROR<<"FFStreamDemuxer::Open,"<<m_error;
				break;
			}else if(avio_feof(m_fmt_ctx->pb)){
				m_error = "av_read_frame avio_feof(m_fmt_ctx->pb)";
				SLOG_ERROR<<"FFStreamDemuxer::Open,"<<m_error;
				break;
			}else if (m_fmt_ctx->pb && m_fmt_ctx->pb->error){
				nomal_exit = false;
				m_error = "av_read_frame m_fmt_ctx->pb && m_fmt_ctx->pb->error";
				SLOG_ERROR<<"FFStreamDemuxer::Open,"<<m_error;
				break;
			}else{
				continue;
			}
		}

		if(m_cb){
			MediaDataBitStream bs;
			if(pkt.stream_index == m_video_index){
				if(!m_current_media_info.live_stream){
					if(first_dts_time < 0){
						first_dts_time = pkt.dts;
					}
					AVRational time_base = m_fmt_ctx->streams[m_video_index]->time_base;
					AVRational time_base_q = { 1,AV_TIME_BASE };
					int64_t dts_delta = av_rescale_q(pkt.dts - first_dts_time, time_base, time_base_q);
					int64_t real_time_delta = av_gettime() - start_time;
					if(dts_delta > real_time_delta)
						av_usleep(dts_delta - real_time_delta);
				}

				if(!m_video_es_buffer)
					m_video_es_buffer = new unsigned char[1024*1024*10];
				memset(m_video_es_buffer,0,1024*1024*10);
				int video_es_buffer_len = 0;
				if(video_bsfc){
					uint8_t* bsfc_buffer = nullptr;
					int bsfc_buffer_len = 0;
					av_bitstream_filter_filter(video_bsfc, video_codec_ctx, nullptr,
								&bsfc_buffer, &bsfc_buffer_len, pkt.data, pkt.size, 0);
					if(bsfc_buffer){
						memcpy(m_video_es_buffer,bsfc_buffer,bsfc_buffer_len);
						video_es_buffer_len = bsfc_buffer_len;
						av_free(bsfc_buffer);
					}else{
						memcpy(m_video_es_buffer,pkt.data,pkt.size);
						video_es_buffer_len = MediaUtils::Avcc2AnnexB(m_video_es_buffer,pkt.size);
					}
				}else{
					memcpy(m_video_es_buffer,pkt.data,pkt.size);
					video_es_buffer_len = pkt.size;
				}
				bs.buffer = m_video_es_buffer;
				bs.buffer_len = video_es_buffer_len;
				bs.media_type = MediaType::VIDEO;
				bs.is_key = pkt.flags & AV_PKT_FLAG_KEY ? true:false;
			}else if(pkt.stream_index == m_audio_index){
				if(!m_audio_es_buffer)
					m_audio_es_buffer = new unsigned char[1024*1024*10];
				int audio_es_buffer_len = 0;
				if(need_create_adts){
					int adts_len = MediaUtils::CreateAdts(m_audio_es_buffer, pkt.size, audio_codec_ctx->extradata, audio_codec_ctx->extradata_size);
					memcpy(m_audio_es_buffer+adts_len,pkt.data,pkt.size);
					audio_es_buffer_len = adts_len+pkt.size;
				}else{
					memcpy(m_audio_es_buffer,pkt.data,pkt.size);
					audio_es_buffer_len = pkt.size;
				}
				bs.buffer = m_audio_es_buffer;
				bs.buffer_len = audio_es_buffer_len;
				bs.media_type = MediaType::AUDIO;
			}
			else{
				av_packet_unref(&pkt);
				continue;
			}

			AVRational output_video_stream_time_base = {1,1000};
			bs.pts = av_rescale_q_rnd(pkt.pts, m_fmt_ctx->streams[pkt.stream_index]->time_base, output_video_stream_time_base, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
			bs.dts = av_rescale_q_rnd(pkt.dts, m_fmt_ctx->streams[pkt.stream_index]->time_base, output_video_stream_time_base, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));

			m_cb(&bs,m_user_data);

		}
		av_packet_unref(&pkt);
	}

	if(video_bsfc)
		av_bitstream_filter_close(video_bsfc);

	if(nomal_exit)
		m_current_status = IOStatus::IDLE;
	else
		m_current_status = IOStatus::ERROR;

	return true;
}

void* FFInputProtocol::WorkThreadFun(void * arg){
	if(!arg)
		return nullptr;
	FFInputProtocol * obj = (FFInputProtocol*)arg;
	obj->StartPull();
	return nullptr;
}

bool FFInputProtocol::Start(){
	m_current_status = IOStatus::RUNNING;
	m_stop = false;
	if(pthread_create(&m_work_thread, nullptr, &WorkThreadFun, this) < 0)
		return false;
	return true;
}

bool FFInputProtocol::Stop(){
	m_stop = true;
	if(m_work_thread > 0){
		pthread_join(m_work_thread,nullptr);
		m_work_thread = 0;
	}
	m_video_index = -1;
	m_audio_index = -1;
	if(m_fmt_ctx){
		avformat_close_input(&m_fmt_ctx);
		m_fmt_ctx = nullptr;
	}
	if(m_video_es_buffer){
		delete []m_video_es_buffer;
		m_video_es_buffer = nullptr;
	}
	if(m_audio_es_buffer){
		delete []m_audio_es_buffer;
		m_audio_es_buffer = nullptr;
	}
	if(m_local_pictrue != ""){
		unlink(m_local_pictrue.c_str());
		m_local_pictrue = "";
	}
	if(m_current_status == IOStatus::RUNNING)
		m_current_status = IOStatus::IDLE;
	return true;
}

