#include "RTMPInputProtocol.h"  

extern "C"{
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/opt.h>
}

#define FLV_BUFFER_SIZE 1024*1024*10
#define AUDIO_BUFFER_SIZE 1024*1024*10
#define VIDEO_BUFFER_SIZE 1024*1024*10

RTMPInputProtocol::RTMPInputProtocol(){
}
	
RTMPInputProtocol::~RTMPInputProtocol(){
	Stop();
}

void RTMPInputProtocol::FLVDemuxerCBFunc(FLVDataType type,unsigned char * buffer,int bufferlen,int64_t pts,int64_t dts,bool key,void * userdata){
    RTMPInputProtocol * obj = (RTMPInputProtocol*)userdata;
    if(obj->m_probe){
        if(type == FLVDataType::TAG_AUDIO){
            memcpy(obj->m_audio_buffer + obj->m_audio_buffer_len,buffer,bufferlen);
            obj->m_audio_buffer_len += bufferlen;
        }else if(type == FLVDataType::TAG_VIDEO_SEQ || type == FLVDataType::TAG_VIDEO){
            memcpy(obj->m_video_buffer + obj->m_video_buffer_len,buffer,bufferlen);
            obj->m_video_buffer_len += bufferlen;
        }
    }else{
        if(type == FLVDataType::TAG_VIDEO_SEQ){
            memcpy(obj->m_video_buffer,buffer,bufferlen);
            obj->m_video_buffer_len = bufferlen;
            return;
        }
        MediaDataBitStream bs;
        bs.dts = dts;
        bs.pts = pts;
        bs.is_key = key;
        if(type == FLVDataType::TAG_AUDIO){
            bs.media_type = MediaType::AUDIO;
            bs.buffer = buffer;
            bs.buffer_len = bufferlen;
            obj->m_cb(&bs,obj->m_user_data);
        }else if(type == FLVDataType::TAG_VIDEO){
            bs.media_type = MediaType::VIDEO;
            if(bs.is_key){
                memcpy(obj->m_video_buffer + obj->m_video_buffer_len,buffer,bufferlen);
                bs.buffer = obj->m_video_buffer;
                bs.buffer_len = obj->m_video_buffer_len + bufferlen;
            }else{
                bs.buffer = buffer;
                bs.buffer_len = bufferlen;
            }
            obj->m_cb(&bs,obj->m_user_data);
        }
    }
}

RTMP * RTMPInputProtocol::ConnectRTMP(char * url){
    RTMP * rtmp = RTMP_Alloc();
    RTMP_Init(rtmp);
    rtmp->Link.timeout = 30;
    
    if(!RTMP_SetupURL(rtmp,url)){
		RTMP_Free(rtmp);
		return nullptr;
	}
    rtmp->Link.lFlags |= RTMP_LF_LIVE;
    RTMP_SetBufferMS(rtmp, 3600*1000);
    if(!RTMP_Connect(rtmp,nullptr)){
		RTMP_Free(rtmp);
		return nullptr;
	}
    if(!RTMP_ConnectStream(rtmp,0)){
		RTMP_Close(rtmp);
		RTMP_Free(rtmp);
		return nullptr;
	}
    return rtmp;
}

void RTMPInputProtocol::CloseRTMP(RTMP * rtmp){
    if(rtmp){
		RTMP_Close(rtmp);
		RTMP_Free(rtmp);
		rtmp = nullptr;
	}
}

void RTMPInputProtocol::FreeMemory(){
    if(m_flv_buffer){
        delete [] m_flv_buffer;
        m_flv_buffer = nullptr;
    }
    if(m_audio_buffer){
        delete [] m_audio_buffer;
        m_audio_buffer = nullptr;
    }
    m_audio_buffer_len = 0;
    if(m_video_buffer){
        delete [] m_video_buffer;
        m_video_buffer = nullptr;
    }
    m_video_buffer_len = 0;
}

void RTMPInputProtocol::AllocMemory(){
    if(!m_flv_buffer){
        m_flv_buffer = new char[FLV_BUFFER_SIZE];
    }
    if(!m_audio_buffer){
        m_audio_buffer = new unsigned char[AUDIO_BUFFER_SIZE];
    }
    m_audio_buffer_len = 0;
    if(!m_video_buffer){
        m_video_buffer = new unsigned char[VIDEO_BUFFER_SIZE];
    }
    m_video_buffer_len = 0;
}

bool RTMPInputProtocol::Open(std::string & source, MediaParams & media_info){
    m_current_status = IOStatus::IDLE;
    m_error = "";
    RTMP * rtmp = ConnectRTMP((char*)source.c_str());
    if(!rtmp){
        return false;
    }
    m_probe = true;

    int byte_read = 0;
    FLVDemuxer demuxer;
    int buffer_len = 0;
    int total_parsed = 0;
    demuxer.Init();
    demuxer.SetDataCB(FLVDemuxerCBFunc,this);
    AllocMemory();
    do{
        byte_read = RTMP_Read(rtmp,m_flv_buffer + buffer_len,FLV_BUFFER_SIZE);
        buffer_len = buffer_len + byte_read;
        int parsed = demuxer.ParseTag((unsigned char*)m_flv_buffer,buffer_len);
        total_parsed += parsed;
        if(total_parsed > 1024*500){
            break;
        }
        buffer_len = buffer_len - parsed;
        memmove(m_flv_buffer,m_flv_buffer + parsed,buffer_len);
    }while(byte_read > 0);

    CloseRTMP(rtmp);

    AVCodec * ff_audio_codec = nullptr;
    FLVAudioCodec audio_codec = demuxer.GetAudioCodec();
    switch (audio_codec){
        case FLVAudioCodec::AAC:
            ff_audio_codec = avcodec_find_decoder(AV_CODEC_ID_AAC);
            media_info.audio.codec = AudioCodec::AAC;
            break;
        case FLVAudioCodec::AC3:
            ff_audio_codec = avcodec_find_decoder(AV_CODEC_ID_AC3);
            media_info.audio.codec = AudioCodec::NONE;
            break;
        case FLVAudioCodec::MP3:
            ff_audio_codec = avcodec_find_decoder(AV_CODEC_ID_MP3);
            media_info.audio.codec = AudioCodec::MP3;
            break;
        case FLVAudioCodec::MP2:
            ff_audio_codec = avcodec_find_decoder(AV_CODEC_ID_MP2);
            media_info.audio.codec = AudioCodec::NONE;
            break;
        default:
            break;
    }
    if(ff_audio_codec){
        AVCodecContext * audio_codec_ctx = avcodec_alloc_context3(ff_audio_codec);
        if (avcodec_open2(audio_codec_ctx, ff_audio_codec, nullptr) < 0) {
		    avcodec_free_context(&audio_codec_ctx);
			return false;
		}
        AVFrame * audio_frame = av_frame_alloc();
        int got_picture = 0;
        AVPacket pkt = {};
        pkt.data = m_audio_buffer;
        pkt.size = m_audio_buffer_len;
        if(avcodec_decode_audio4(audio_codec_ctx, audio_frame, &got_picture, &pkt) < 0){
            avcodec_close(audio_codec_ctx);
		    avcodec_free_context(&audio_codec_ctx);
            return false;
        }
        media_info.audio.bit_rate = audio_codec_ctx->bit_rate;
        media_info.audio.channels = audio_codec_ctx->channels;
        media_info.audio.sample_rate = audio_codec_ctx->sample_rate;
        media_info.audio.sample_size = audio_codec_ctx->frame_size;
        avcodec_close(audio_codec_ctx);
        avcodec_free_context(&audio_codec_ctx);
        av_frame_free(&audio_frame);
    }

    AVCodec * ff_video_codec = nullptr;
    FLVVideoCodec video_codec = demuxer.GetVideoCodec();
    switch (video_codec){
        case FLVVideoCodec::H264:
            ff_video_codec = avcodec_find_decoder(AV_CODEC_ID_H264);
            media_info.video.codec = VideoCodec::H264;
            break;
        case FLVVideoCodec::HEVC:
            ff_video_codec = avcodec_find_decoder(AV_CODEC_ID_HEVC);
            media_info.video.codec = VideoCodec::HEVC;
            break;
        default:
            break;
    }

    if(ff_video_codec){
        AVCodecContext * video_codec_ctx = avcodec_alloc_context3(ff_video_codec);
        if (avcodec_open2(video_codec_ctx, ff_video_codec, nullptr) < 0) {
		    avcodec_free_context(&video_codec_ctx);       
			return false;
		}
        AVFrame * video_frame = av_frame_alloc();
        int got_picture = 0;
        AVPacket pkt = {};
        pkt.data = m_video_buffer;
        pkt.size = m_video_buffer_len;
        if(avcodec_decode_video2(video_codec_ctx, video_frame, &got_picture, &pkt) < 0){
            avcodec_close(video_codec_ctx);
		    avcodec_free_context(&video_codec_ctx);
            return false;
        }
        media_info.video.width = video_codec_ctx->width;
        media_info.video.height = video_codec_ctx->height;
        media_info.video.bit_rate = video_codec_ctx->bit_rate;
        media_info.video.gop_size = video_codec_ctx->gop_size;
        media_info.video.frame_rate_den = video_codec_ctx->framerate.den;
        media_info.video.frame_rate_num = video_codec_ctx->framerate.num;
        switch (video_codec_ctx->pix_fmt) {
            case AV_PIX_FMT_YUV420P:
                media_info.video.pix_fmt = VideoBaseBandFmt::YUV420P;
                break;
            default:
                media_info.video.pix_fmt = VideoBaseBandFmt::YUV420P;
                break;
        }
        avcodec_close(video_codec_ctx);
        avcodec_free_context(&video_codec_ctx);
        av_frame_free(&video_frame);
    }
    media_info.muxer_type = MuxerType::FLV;
    m_source = source;
    return true;
}

bool RTMPInputProtocol::Pull(){
    m_current_status = IOStatus::RUNNING;
    RTMP * rtmp = ConnectRTMP((char*)m_source.c_str());
    if(!rtmp){
         m_current_status = IOStatus::ERROR;
         m_error = "ConnectRTMP error";
         return false;
    }
    int byte_read = 0;
    FLVDemuxer demuxer;
    int buffer_len = 0;
    demuxer.Init();
    demuxer.SetDataCB(FLVDemuxerCBFunc,this);
    AllocMemory();
    do{
        byte_read = RTMP_Read(rtmp,m_flv_buffer + buffer_len,FLV_BUFFER_SIZE);
        if(byte_read <= 0){
            CloseRTMP(rtmp);
            m_current_status = IOStatus::ERROR;
            m_error = "RTMP_Read error";
            return false;
        }
        buffer_len = buffer_len + byte_read;
        int parsed = demuxer.ParseTag((unsigned char*)m_flv_buffer,buffer_len);
        buffer_len = buffer_len - parsed;
        memmove(m_flv_buffer,m_flv_buffer + parsed,buffer_len);
    }while(!m_stop);
    CloseRTMP(rtmp);
    m_current_status = IOStatus::IDLE;
    return true;
}

void * RTMPInputProtocol::WorkThreadFunc(void * arg){
    RTMPInputProtocol * obj = (RTMPInputProtocol*)arg;
    obj->Pull();
    return nullptr;
}

bool RTMPInputProtocol::Start(){
    if(m_worker_thread == 0){
        m_probe = false;
        m_stop = false;
        pthread_create(&m_worker_thread,nullptr,WorkThreadFunc,this);
    }
    return true;
}
bool RTMPInputProtocol::Stop(){
    if(m_worker_thread > 0){
        m_stop = true;
        pthread_join(m_worker_thread,0);
        m_worker_thread = 0;
    }
    FreeMemory();
    m_source = "";
    return true;
}