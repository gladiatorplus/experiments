
#ifndef FFINPUTPROTOCOL_H_
#define FFINPUTPROTOCOL_H_


#include "../../Media.h"


class FFInputProtocol : public InputProtocol{
public:
	FFInputProtocol();
	virtual ~FFInputProtocol();
	virtual bool Open(std::string & source, MediaParams & media_info);
	virtual bool Start();
	virtual bool Stop();
private:
	int m_video_index = -1;
	int m_audio_index = -1;
	pthread_t m_work_thread = 0;
	struct AVFormatContext *m_fmt_ctx = nullptr;
	unsigned char * m_video_es_buffer = nullptr;
	unsigned char * m_audio_es_buffer = nullptr;
	MediaParams m_current_media_info;
	std::string m_source_url;
	std::string m_local_pictrue;
private:
	//Control
	bool m_stop = false;
	time_t m_timeout_start_time = 0;
private:
	bool StartPull();
	static void* WorkThreadFun(void * arg);
	static int InterruptCB(void *arg);
};


#endif
