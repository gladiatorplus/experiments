
#include "FFOutputProtocol.h"
extern "C" {
	#include "libavformat/avformat.h"
	#include "libavutil/opt.h"
}

FFOutputProtocol::FFOutputProtocol() {
}

FFOutputProtocol::~FFOutputProtocol() {
	Stop();
}

struct HLSInfo{
	std::string file_location = "";
	std::string time = "10";
	std::string list_size = "10";
	std::string wrap = "10";
	std::string segment_type = "ts";
};

static bool AnalyseHlsUrl(std::string & url,HLSInfo & info){

	std::string::size_type option_pos = url.find("?");
	if(option_pos != std::string::npos){
		info.file_location = url.substr(6,option_pos - 6);
		option_pos = url.find("hls_time");
		std::string::size_type option_last_pos = 0;
		std::string option = "";
		std::string option_val = "";
		if(option_pos != std::string::npos){
			option_last_pos = url.find("&",option_pos);
			option = url.substr(option_pos,option_last_pos - option_pos);
			option_val = option.substr(option.find("=") + 1,option.length() - option.find("=") - 1);
			info.time = option_val;
		}
		option_pos = url.find("hls_list_size");
		if(option_pos != std::string::npos){
			option_last_pos = url.find("&",option_pos);
			option = url.substr(option_pos,option_last_pos - option_pos);
			option_val = option.substr(option.find("=") + 1,option.length() - option.find("=") - 1);
			info.list_size = option_val;
		}
		option_pos = url.find("hls_wrap");
		if(option_pos != std::string::npos){
			option_last_pos = url.find("&",option_pos);
			option = url.substr(option_pos,option_last_pos - option_pos);
			option_val = option.substr(option.find("=") + 1,option.length() - option.find("=") - 1);
			info.wrap = option_val;
		}
		option_pos = url.find("hls_segment_type");
		if(option_pos != std::string::npos){
			option_last_pos = url.find("&",option_pos);
			option = url.substr(option_pos,option_last_pos - option_pos);
			option_val = option.substr(option.find("=") + 1,option.length() - option.find("=") - 1);
			info.segment_type = option_val;
		}
	}else{
		info.file_location = url.substr(6);
	}
	if(info.file_location[info.file_location.length() - 1] != '/'){
		info.file_location += "/";
	}
	if(info.file_location != ""){
		info.file_location += "index.m3u8";
	}
	return true;
}


bool FFOutputProtocol::Start(std::string & url,MediaParams & params){
	Stop();

	if(url.find("hls://") != std::string::npos){
		HLSInfo info;
		AnalyseHlsUrl(url,info);
		if (CreateFullPath(info.file_location) < 0){
			if (EEXIST != errno) {
				return false;
			}
		}
		url = info.file_location;
		avformat_alloc_output_context2(&m_fmt_ctx, nullptr, "hls", info.file_location.c_str());
		av_opt_set(m_fmt_ctx->priv_data, "hls_time", info.time.c_str(), AV_OPT_SEARCH_CHILDREN);
		av_opt_set(m_fmt_ctx->priv_data, "hls_list_size", info.list_size.c_str(), AV_OPT_SEARCH_CHILDREN);
		av_opt_set(m_fmt_ctx->priv_data, "hls_wrap", info.wrap.c_str(), AV_OPT_SEARCH_CHILDREN);
		av_opt_set(m_fmt_ctx->priv_data, "hls_segment_type", info.segment_type.c_str(), AV_OPT_SEARCH_CHILDREN);
	}else{
		if (CreateFullPath(url) < 0){
			if (EEXIST != errno) {
				return false;
			}
		}
		avformat_alloc_output_context2(&m_fmt_ctx, nullptr, nullptr, url.c_str());
	}

	if(!m_fmt_ctx)
		return false;

	memcpy(&m_media_params,&params,sizeof(MediaParams));

	if (params.video.codec > VideoCodec::NONE) {
		m_video_stream = avformat_new_stream(m_fmt_ctx, nullptr);
		m_video_stream->id = m_fmt_ctx->nb_streams - 1;
		m_video_stream->codec->codec_type = AVMEDIA_TYPE_VIDEO;
		m_video_stream->codec->width = m_media_params.video.width;
		m_video_stream->codec->height = m_media_params.video.height;
		m_video_stream->codec->codec_tag = 0;
		m_video_stream->codec->pix_fmt = AV_PIX_FMT_YUV420P;
		m_video_stream->codec->bit_rate = m_media_params.video.bit_rate;
		if (params.video.codec == VideoCodec::H264) {
			m_video_stream->codec->codec_id = AV_CODEC_ID_H264;
		}
		else if (params.video.codec == VideoCodec::HEVC) {
			m_video_stream->codec->codec_id = AV_CODEC_ID_HEVC;
		}
		else
			return false;

	}

	if (params.audio.codec > AudioCodec::NONE) {
		m_audio_stream = avformat_new_stream(m_fmt_ctx, nullptr);
		m_audio_stream->codec->codec_type = AVMEDIA_TYPE_AUDIO;
		m_audio_stream->codec->channels = m_media_params.audio.channels;
		m_audio_stream->codec->sample_rate = m_media_params.audio.sample_rate;
		m_audio_stream->codec->codec_tag = 0;
		m_audio_stream->codec->bit_rate = m_media_params.audio.bit_rate;
		if (params.audio.codec == AudioCodec::AAC)
			m_audio_stream->codec->codec_id = AV_CODEC_ID_AAC;
		else if(params.audio.codec == AudioCodec::MP3)
			m_audio_stream->codec->codec_id = AV_CODEC_ID_MP3;
		else
			return false;
	}

	if (avio_open(&m_fmt_ctx->pb, url.c_str(), AVIO_FLAG_WRITE) < 0)
		return false;

	av_dump_format(m_fmt_ctx, 0, url.c_str(), 1);

	if (avformat_write_header(m_fmt_ctx, nullptr) < 0)
		return false;
	m_status = IOStatus::RUNNING;
	return true;
}
bool FFOutputProtocol::InputData(MediaData * data){
	MediaDataBitStream * bs = static_cast<MediaDataBitStream *>(data);

	if (!m_fmt_ctx || !m_video_stream  || !bs->buffer || bs->buffer_len <= 0)
		return false;

	AVPacket pkt;
	av_init_packet(&pkt);
	pkt.data = bs->buffer;
	pkt.size = bs->buffer_len;
	AVRational input_stream_timebase = { 1,1000 };
	if(bs->media_type == MediaType::VIDEO){
		pkt.pts = av_rescale_q_rnd(bs->pts, input_stream_timebase, m_video_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
		pkt.dts = av_rescale_q_rnd(bs->dts, input_stream_timebase, m_video_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
		pkt.stream_index = m_video_stream->index;
		bs->is_key ? pkt.flags |= AV_PKT_FLAG_KEY : pkt.flags;
	}
	else if(bs->media_type == MediaType::AUDIO){
		pkt.pts = av_rescale_q_rnd(bs->pts, input_stream_timebase, m_audio_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
		pkt.dts = av_rescale_q_rnd(bs->dts, input_stream_timebase, m_audio_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
		pkt.stream_index = m_audio_stream->index;
	}
	else
		return false;


	int ret = av_interleaved_write_frame(m_fmt_ctx, &pkt);
	if (ret < 0){
		m_status = IOStatus::ERROR;
		return false;
	}else{
		m_status = IOStatus::RUNNING;
		return true;
	}
}
bool FFOutputProtocol::Stop(){
	if (m_fmt_ctx) {
		if (m_fmt_ctx->pb) {
			av_write_trailer(m_fmt_ctx);
			avio_close(m_fmt_ctx->pb);
		}
		avformat_free_context(m_fmt_ctx);
		m_fmt_ctx = nullptr;
	}
	m_status = IOStatus::IDLE;
	return true;
}
