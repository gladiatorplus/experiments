
#ifndef FFOUTPUTPROTOCOL_H_
#define FFOUTPUTPROTOCOL_H_

#include "../../Media.h"

class FFOutputProtocol : public OutputProtocal{
public:
	FFOutputProtocol();
	virtual ~FFOutputProtocol();
	virtual bool Start(std::string & url,MediaParams & params);
	virtual bool InputData(MediaData * data);
	virtual bool Stop();
private:
	struct AVFormatContext * m_fmt_ctx = nullptr;
	struct AVStream * m_video_stream = nullptr;
	struct AVStream * m_audio_stream = nullptr;
	MediaParams m_media_params;
};

#endif 
