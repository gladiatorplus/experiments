
#ifndef RTMPOUTPUTPROTOCOL_H_
#define RTMPOUTPUTPROTOCOL_H_

extern "C"{
	#include "rtmp.h"
};

#include "../../Media.h"
#include "../../Format/FLV/FLVMuxer.h"

class RTMPOutputProtocol : public OutputProtocal{
public:
	RTMPOutputProtocol();
	virtual ~RTMPOutputProtocol();
	virtual bool Start(std::string & url,MediaParams & params);
	virtual bool InputData(MediaData * data);
	virtual bool Stop();
private:
	FLVMuxer * flv_muxer = nullptr;
	static pthread_mutex_t m_thread_lock;
	RTMP * m_rtmp = nullptr;
	std::string m_url;
	MediaParams m_media_params;
	unsigned char * m_flv_meta_buffer = nullptr;
	int m_flv_meta_buffer_len = 0;
	bool m_meta_sent = false;
private:
	static void FLVDataCB(FLVDataType type,unsigned char * buffer,int buffer_len,void * user_data);
	bool PushRTMP(FLVDataType type,unsigned char * data,int data_len);
	bool ConnectRTMP();
	void CloseRTMP();
};

#endif 
