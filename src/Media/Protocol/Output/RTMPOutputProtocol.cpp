
#include "RTMPOutputProtocol.h"

pthread_mutex_t RTMPOutputProtocol::m_thread_lock = PTHREAD_MUTEX_INITIALIZER;

RTMPOutputProtocol::RTMPOutputProtocol() {
}

RTMPOutputProtocol::~RTMPOutputProtocol() {
	Stop();
}


void RTMPOutputProtocol::FLVDataCB(FLVDataType type,unsigned char * buffer,int buffer_len,void * user_data){
	if(!user_data)
		return;
	RTMPOutputProtocol * obj = (RTMPOutputProtocol*)user_data;
	obj->PushRTMP(type,buffer,buffer_len);
}

bool RTMPOutputProtocol::PushRTMP(FLVDataType type,unsigned char * data,int data_len){

	if(m_rtmp && RTMP_IsConnected(m_rtmp)){
		if(type == FLVDataType::HEAD || type == FLVDataType::TAG_SCRIPT){
			memcpy(m_flv_meta_buffer + m_flv_meta_buffer_len,data,data_len);
			m_flv_meta_buffer_len += data_len;
		}else if(type == FLVDataType::TAG_AUDIO ||
				type == FLVDataType::TAG_AUDIO_SEQ ||
				type == FLVDataType::TAG_VIDEO ||
				type == FLVDataType::TAG_VIDEO_SEQ){
			AutoLock auto_lock(&m_thread_lock);
			if(!m_meta_sent){
				if (RTMP_Write(m_rtmp, (const char*)m_flv_meta_buffer, m_flv_meta_buffer_len) < 0) {
					SLOG_ERROR<<"RTMPOutputProtocol::PushRTMP, RTMP_Write error";
					return false;
				}
				m_meta_sent = true;
				m_flv_meta_buffer_len = 0;
			}
			if (RTMP_Write(m_rtmp, (const char*)data, data_len) < 0) {
				SLOG_ERROR<<"RTMPOutputProtocol::PushRTMP, RTMP_Write error";
				return false;
			}
		}
	}else{
		CloseRTMP();
		if(ConnectRTMP()){
			m_meta_sent = false;
			m_flv_meta_buffer_len = 0;
			flv_muxer->Init(m_media_params);
		}
	}

	return true;
}

void RTMPOutputProtocol::CloseRTMP(){
	AutoLock auto_lock(&m_thread_lock);
	if(m_rtmp){
		if (RTMP_IsConnected(m_rtmp)) {
			RTMP_Close(m_rtmp);
		}
		RTMP_Free(m_rtmp);
		m_rtmp = nullptr;
	}
}

bool RTMPOutputProtocol::ConnectRTMP(){

	AutoLock auto_lock(&m_thread_lock);
	if(!m_rtmp)
		m_rtmp = RTMP_Alloc();

	RTMP_Init(m_rtmp);
	m_rtmp->Link.timeout = 5;

	if (!RTMP_SetupURL(m_rtmp, (char*) m_url.c_str())) {
		m_status = IOStatus::ERROR;
		SLOG_ERROR<<"RTMPOutputProtocol::ConnectRTMP, RTMP_SetupURL error";
		return false;
	}

	RTMP_EnableWrite(m_rtmp);

	RTMP_SetBufferMS(m_rtmp, 10 * 60 * 60 * 1000);

	if (!RTMP_Connect(m_rtmp, nullptr)) {
		m_status = IOStatus::ERROR;
		SLOG_ERROR<<"RTMPOutputProtocol::ConnectRTMP, RTMP_Connect error";
		return false;
	}

	if (!RTMP_ConnectStream(m_rtmp, 0)) {
		m_status = IOStatus::ERROR;
		SLOG_ERROR<<"RTMPOutputProtocol::ConnectRTMP, RTMP_ConnectStream error";
		return false;
	}
	m_status = IOStatus::RUNNING;
	return true;
}

bool RTMPOutputProtocol::Start(std::string & url,MediaParams & params){
	m_url = url;
	CloseRTMP();
	if(!ConnectRTMP()){
		CloseRTMP();
		return false;
	}
	m_meta_sent = false;

	if(!m_flv_meta_buffer)
		m_flv_meta_buffer = new unsigned char[1024*1024*10];
	m_flv_meta_buffer_len = 0;

	if(flv_muxer){
		delete flv_muxer;
		flv_muxer = nullptr;
	}
	flv_muxer = new FLVMuxer();
	flv_muxer->SetDataCB(FLVDataCB,this);
	flv_muxer->Init(params);
	memcpy(&m_media_params,&params,sizeof(MediaParams));
	m_status = IOStatus::RUNNING;
	return true;
}
bool RTMPOutputProtocol::InputData(MediaData * data){
	if(!flv_muxer)
		return false;
	return flv_muxer->InputData(data);
}
bool RTMPOutputProtocol::Stop(){
	if(flv_muxer){
		delete flv_muxer;
		flv_muxer = nullptr;
	}
	if(m_flv_meta_buffer){
		delete []m_flv_meta_buffer;
		m_flv_meta_buffer = nullptr;
	}
	CloseRTMP();
	if(m_status == IOStatus::RUNNING)
		m_status = IOStatus::IDLE;
	return true;
}
