# 我们使用的是 MultiStage Build 
# 具体参考: https://docs.docker.com/develop/develop-images/multistage-build/

# Step1: 启动一个临时的 builder 容器
#        我们直接从 builder 里面拷贝出依赖项  
# 这个镜像的构建参考 Dockerfile.builder 
# 

FROM registry.cn-beijing.aliyuncs.com/cmc/sm:ingress-builder-1.0.0-10-g09cd13e AS builder

ADD . /root/storage


RUN echo "Building Ingress"  && \
rm -fr /root/storage/built/node && \
mkdir -p /root/storage/built/node && \
cd /root/storage/built/node && \
cmake -DBOOST_ROOT=/root/deps/Boost/  \
-DOpenCV_DIR=/root/deps/OpenCV/lib/cmake/opencv4 \
-DFFMPEG_PREFIX=/root/deps/FFmpeg/  \
-DRAPIDJSON_PREFIX=/root/deps/rapidjson/  \
-DCMAKE_BUILD_TYPE=RelWithDebInfo  \
-DLIBRDKAFKA_INCLUDE_DIR=/root/deps/librdkafka/include \
-DLIBRDKAFKA_LIB_DIR=/root/deps/librdkafka/lib \
/root/storage  && make VERBOSE=1 -j $(getconf _NPROCESSORS_ONLN)



FROM golang:1.15 as ProcessControllerBuilder 
WORKDIR /go/src/
COPY ProcessController .
RUN export GOPROXY=https://goproxy.io,direct \
 && GOFLAGS=-mod=vendor go build -gcflags=all="-N -l" -o processController



# Step2: 构建 monitor Node 容器.
# 
#
# 这个镜像的构建参考 dep/ubuntu-nginx-rtmp-docker  
FROM  registry.cn-beijing.aliyuncs.com/cmc/qsc:nginx-rtmp-0.0.2-0-g5d7ea70

# 基本的依赖项  
RUN apt-get update && apt-get install -y gdb  libx264-dev  libssl1.0-dev 

# 从 Builder 里面拷贝依赖项  
COPY --from=builder /root/deps/Boost/lib /root/deps/Boost/lib
COPY --from=builder /root/deps/OpenCV/lib /root/deps/OpenCV/lib
COPY --from=builder /root/deps/FFmpeg/lib /root/deps/FFmpeg/lib
COPY --from=builder /root/deps/librdkafka/lib /root/deps/librdkafka/lib 
COPY --from=builder  /root/storage/built/node/snow-moutain-ingress /sbin/snow-moutain-ingress
COPY --from=ProcessControllerBuilder /go/src/processController /usr/bin


ADD nginx.conf.tmpl /etc/nginx/nginx.conf.tmpl
ADD entry.sh  /bin/entry.sh 

RUN echo 'export LD_LIBRARY_PATH=/root/deps/FFmpeg/lib:/root/deps/OpenCV/lib:/root/deps/Boost/lib' >> /root/.bashrc


VOLUME /data

#
# 添加一些默认的环境变量 
#

ENV SOURCE_STREAM_URL=rtmp://livepgc.chinamcache.com/pgc/607175c62bace8079f8510de02d17de8?auth_key=1632208057-0-0-96900ceca9284d285e654d9799b2d4bb 
ENV CALLBACK_URL http://localhost:18080/mock/callback 
ENV NODE_SELECTORS "*"
ENV OUTPUT_RESOLUTION 640:480 
ENV OUTPUT_VIDEO_BITRATE 200 
ENV OUTPUT_AUDIO_BITRATE 96 
ENV OUTPUT_VFRAME_RATE 25 
ENV OUTPUT_ASAMPLE_RATE 48000 
ENV MONITOR_NODE_ID unknown 
ENV SNAPSHOT_INGRESS_URL http://xxx/xxx/image  
ENV PREVIEW_STREAM_INGRESS_URL rtmp://xxx/xxx/xx 
ENV OUTPUT_STREAM_URL rtmp://127.0.0.1/live/preview 
ENV WORK_DIR /data
ENV KAFKA_BROKERS_LIST qsc3.chinamcloud.cn:8082
ENV LISTEN_PORT 38080


#CMD ["sleep","infinity"]
CMD ["bash","/bin/entry.sh"]
